FROM adoptopenjdk/openjdk11:jre

LABEL maintainer="BadToxic"
LABEL description="GameMaster Kubernetes Discord Bot"

ADD target/gm_kub-thorntail.jar /opt/thorntail.jar
ADD backup/project-defaults-local.yml /etc/config/project-defaults.yml

EXPOSE 8080

ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5006", "-jar", "/opt/thorntail.jar", "-s", "/etc/config/project-defaults.yml", "-Djava.net.preferIPv4Stack=true"]