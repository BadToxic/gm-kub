package mapper;

import de.badtoxic.gm_kub.dto.ItemDTO;
import de.badtoxic.gm_kub.entity.Item;
import de.badtoxic.gm_kub.mapper.ItemMapper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class TestMapperItem extends TestMapperBase {

    @Test
    public void testMapperItem() {

        Item gameGuyWhite = new Item(1, "GameGuy White", "https://i.imgur.com/uyETMJO.png",
                "Portable game device with low specs.");
        Item gameGuyCartridgeGong = new Item(2, "GameGuy Cartridge Gong", "https://i.imgur.com/IeIqfJw.png",
                "Game modul for the GameGuy handheld console with the game Gong.");

        ItemDTO gameGuyWhiteDTO = ItemMapper.INSTANCE.toDTO(gameGuyWhite);
        ItemDTO gameGuyCartridgeGongDTO = ItemMapper.INSTANCE.toDTO(gameGuyCartridgeGong);

        assertItemMapping(gameGuyWhite, gameGuyWhiteDTO);
        assertItemMapping(gameGuyCartridgeGong, gameGuyCartridgeGongDTO);
    }

    private void assertItemMapping(Item item, ItemDTO itemDTO) {
        assertBaseMapping(item, itemDTO);
        assertEquals(item.getDescription(), itemDTO.getDescription());
    }
}