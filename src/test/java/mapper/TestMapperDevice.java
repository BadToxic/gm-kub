package mapper;

import de.badtoxic.gm_kub.dto.DeviceDTO;
import de.badtoxic.gm_kub.entity.Device;
import de.badtoxic.gm_kub.mapper.DeviceMapper;
import org.junit.Test;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class TestMapperDevice extends TestMapperBase {

    @Test
    public void testMapperDevice() {

        Device gameGuy = new Device(1, "GameGuy", "https://i.imgur.com/uyETMJO.png");

        DeviceDTO gameGuyDTO = DeviceMapper.INSTANCE.toDTO(gameGuy);

        assertDeviceMapping(gameGuy, gameGuyDTO);
    }

    private void assertDeviceMapping(Device device, DeviceDTO deviceDTO) {
        assertBaseMapping(device, deviceDTO);
    }
}