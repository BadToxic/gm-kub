package mapper;

import de.badtoxic.gm_kub.dto.MiniGameDTO;
import de.badtoxic.gm_kub.entity.MiniGame;
import de.badtoxic.gm_kub.mapper.MiniGameMapper;
import org.junit.Test;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class TestMapperMiniGame extends TestMapperBase {

    @Test
    public void testMapperMiniGame() {

        MiniGame gong = new MiniGame(1, "Gong", "https://i.imgur.com/0Bbzxau.png");
        MiniGame pairs = new MiniGame(2, "Pairs", "https://i.imgur.com/StRUHav.png");
        MiniGame fizhy = new MiniGame(3, "Fizhy", "https://i.imgur.com/YSgHqfp.png");

        MiniGameDTO gongDTO = MiniGameMapper.INSTANCE.toDTO(gong);
        MiniGameDTO pairsDTO = MiniGameMapper.INSTANCE.toDTO(pairs);
        MiniGameDTO fizhyDTO = MiniGameMapper.INSTANCE.toDTO(fizhy);

        assertMiniGameMapping(gong, gongDTO);
        assertMiniGameMapping(pairs, pairsDTO);
        assertMiniGameMapping(fizhy, fizhyDTO);
    }

    private void assertMiniGameMapping(MiniGame miniGame, MiniGameDTO miniGameDTO) {
        assertBaseMapping(miniGame, miniGameDTO);
    }
}