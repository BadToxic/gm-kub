package mapper;

import de.badtoxic.gm_kub.dto.BaseDTO;
import de.badtoxic.gm_kub.entity.BaseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class TestMapperBase {

    protected void assertBaseMapping(BaseEntity entity, BaseDTO dto) {
        assertNotNull(dto);
        assertEquals(dto.getId(), entity.getId());
        assertEquals(dto.getName(), entity.getName());
    }
}