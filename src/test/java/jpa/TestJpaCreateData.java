package jpa;

import de.badtoxic.gm_kub.entity.MiniGame;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class TestJpaCreateData {

    private EntityManager em;

    @Test
    public void testCreateData() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GMKubPUTest");
        em = emf.createEntityManager();

        createMiniGame(1, "Gong", "https://i.imgur.com/0Bbzxau.png");
        createMiniGame(2, "Pairs", "https://i.imgur.com/StRUHav.png");
        createMiniGame(3, "Fizhy", "https://i.imgur.com/YSgHqfp.png");
    }

    private void createMiniGame(int id, String name, String image) {
        MiniGame miniGame = em.find(MiniGame.class, id);
        if (miniGame != null) {
            System.out.println("Game with id " + id + " already exists. (Name: " + name + ")");
            return;
        }
        em.getTransaction().begin();
        miniGame = new MiniGame(id, name, image);
        em.persist(miniGame);
        em.getTransaction().commit();
        System.out.println("Created game \"" + name  + "\" with id: " + id + ".");
    }
}