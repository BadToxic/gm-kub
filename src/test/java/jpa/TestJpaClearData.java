package jpa;

import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class TestJpaClearData {
    private EntityManager em;

    @Test
    public void testClearData() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GMKubPUTest");
        em = emf.createEntityManager();

        clearMiniGames();
    }

    private void clearMiniGames() {
        em.getTransaction().begin();
        em.createQuery("DELETE FROM MiniGame").executeUpdate();
        em.getTransaction().commit();
        System.out.println("Cleared mini game table.");
    }
}