package de.badtoxic.gm_kub.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import de.badtoxic.gm_kub.dto.DeviceDTO;
import de.badtoxic.gm_kub.dto.ItemDTO;
import de.badtoxic.gm_kub.dto.MiniGameDTO;
import de.badtoxic.gm_kub.mapper.DeviceMapper;
import de.badtoxic.gm_kub.mapper.ItemMapper;
import de.badtoxic.gm_kub.mapper.MiniGameMapper;
import de.badtoxic.gm_kub.repository.DeviceRepository;
import de.badtoxic.gm_kub.repository.ItemRepository;
import de.badtoxic.gm_kub.repository.MiniGameRepository;

import javax.inject.Inject;

/**
 * GraphQL query
 * (Use via CDI breaks GraphQL library, this has to be instantiated "directly".)
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class GraphQLQuery implements GraphQLQueryResolver {

    private MiniGameRepository miniGameRepository;
    private DeviceRepository deviceRepository;
    private ItemRepository itemRepository;

    @Inject
    public GraphQLQuery(MiniGameRepository miniGameRepository,
                        DeviceRepository deviceRepository,
                        ItemRepository itemRepository) {
        this.miniGameRepository = miniGameRepository;
        this.deviceRepository = deviceRepository;
        this.itemRepository = itemRepository;
    }

    public MiniGameDTO getMiniGame(int id) {
        return MiniGameMapper.INSTANCE.toDTO(miniGameRepository.find(id));
    }
    public MiniGameDTO getMiniGameByName(String name) {
        return MiniGameMapper.INSTANCE.toDTO(miniGameRepository.findByName(name));
    }

    public DeviceDTO getDevice(int id) {
        return DeviceMapper.INSTANCE.toDTO(deviceRepository.find(id));
    }

    public ItemDTO getItem(int id) {
        return ItemMapper.INSTANCE.toDTO(itemRepository.find(id));
    }

}
