package de.badtoxic.gm_kub.graphql;

import graphql.ErrorType;
import graphql.ExecutionResult;
import graphql.GraphQLError;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * GraphQL end point
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@RequestScoped
public class GraphQLEndPoint {

    private GraphQLBase graphQLBase;

    public GraphQLEndPoint() {}

    @Inject
    public GraphQLEndPoint(GraphQLBase graphQLBase) {
        this.graphQLBase = graphQLBase;
    }

    public Response search(String graphQLQuery) {
        return parseResult(graphQLBase.executeSearch(graphQLQuery));
    }

    private Response parseResult(ExecutionResult result) {
        List<GraphQLError> graphQLErrors = result.getErrors();
        if (!graphQLErrors.isEmpty()) {
            return handleErrors(graphQLErrors);
        }
        return Response.ok(result).build();
    }

    private static final List<ErrorType> REQUEST_ERRORS = List.of(ErrorType.InvalidSyntax, ErrorType.ValidationError, ErrorType.OperationNotSupported);

    private Response handleErrors(List<GraphQLError> errors) {
        List<GraphQLError> requestErrors = new ArrayList<>();
        List<GraphQLError> internalErrors = new ArrayList<>();
        errors.forEach(
                error -> {
                    if (REQUEST_ERRORS.contains((ErrorType) error.getErrorType())) {
                        requestErrors.add(error);
                    } else {
                        internalErrors.add(error);
                    }
                }
        );
        // if there are no errors in the sent request, return 500; otherwise any server errors may be due to bad request, so sent only those
        return requestErrors.isEmpty() ? Response.serverError().entity(internalErrors).build()
                : Response.status(400).entity(requestErrors).build();
    }
}
