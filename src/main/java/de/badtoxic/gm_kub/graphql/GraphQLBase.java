package de.badtoxic.gm_kub.graphql;

import com.coxautodev.graphql.tools.SchemaParser;
import com.coxautodev.graphql.tools.SchemaParserBuilder;
import de.badtoxic.gm_kub.repository.DeviceRepository;
import de.badtoxic.gm_kub.repository.ItemRepository;
import de.badtoxic.gm_kub.repository.MiniGameRepository;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.regex.Pattern;

/**
 * GraphQL base object
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@ApplicationScoped
public class GraphQLBase {
//    private DataFetcherExceptionHandler exceptionHandlingStrategy;
    private GraphQL graphQL;

    private static final Pattern PARAMETER_PATTERN = Pattern.compile(".*(?<param>\\$\\w*).*");
    private static final Logger LOG = LoggerFactory.getLogger(GraphQLBase.class);

    private MiniGameRepository miniGameRepository;
    private DeviceRepository deviceRepository;
    private ItemRepository itemRepository;

    protected GraphQLBase() {}

    @Inject
    public GraphQLBase(/*@AllowPartialExecution DataFetcherExceptionHandler exceptionHandlingStrategy*/
            MiniGameRepository miniGameRepository,
            DeviceRepository deviceRepository,
            ItemRepository itemRepository) {
        // this.exceptionHandlingStrategy = exceptionHandlingStrategy;
        this.miniGameRepository = miniGameRepository;
        this.deviceRepository = deviceRepository;
        this.itemRepository = itemRepository;
    }

    @PostConstruct
    public void initialiseGraphQL() {

        SchemaParserBuilder file = SchemaParser.newParser().file("schema.graphqls");

        SchemaParserBuilder withResolvers = file.resolvers(
                new GraphQLQuery(miniGameRepository, deviceRepository, itemRepository));
        SchemaParser withBuild = withResolvers.build();
        GraphQLSchema graphQLSchema = withBuild.makeExecutableSchema();

        graphQL = GraphQL.newGraphQL(graphQLSchema)
//                .queryExecutionStrategy(new AsyncExecutionStrategy(exceptionHandlingStrategy))
                .build();
    }

    /**
     * @param queryString The query to be executed.
     * @return The execution result for the query.
     */
    public ExecutionResult executeSearch(String queryString) {
        return graphQL.execute(queryString);
    }

}
