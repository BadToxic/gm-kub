package de.badtoxic.gm_kub.repository;

import javax.persistence.*;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class BaseRepository {

    @PersistenceContext(unitName="GMKubPU")
    protected static EntityManager em;

    public Object find(Class entityClass, int id) {
        return em.find(entityClass, id);
    }
}
