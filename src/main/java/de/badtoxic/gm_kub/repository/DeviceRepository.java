package de.badtoxic.gm_kub.repository;

import de.badtoxic.gm_kub.entity.Device;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@ApplicationScoped
public class DeviceRepository extends BaseRepository {

    @Transactional
    public Device find(int id) {
        return (Device)super.find(Device.class, id);
    }

    @Transactional
    public List<Device> findAll() {
        return em.createNamedQuery("Device.findAll", Device.class).getResultList();
    }

    @Transactional
    public Device findByName(String name) {
        try {
            return em.createNamedQuery("Device.findByName", Device.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch(Exception exception) {
            return null;
        }
    }
}
