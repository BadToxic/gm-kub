package de.badtoxic.gm_kub.repository;

import de.badtoxic.gm_kub.entity.Item;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@Transactional
@ApplicationScoped
public class ItemRepository extends BaseRepository {

    public Item find(int id) {
        return (Item)super.find(Item.class, id);
    }

    public List<Item> findAll() {
        return em.createNamedQuery("Item.findAll", Item.class).getResultList();
    }

    public Item findByName(String name) {
        try {
            return em.createNamedQuery("Item.findByName", Item.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch(Exception exception) {
            return null;
        }
    }
}
