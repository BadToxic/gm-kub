package de.badtoxic.gm_kub.repository;

import de.badtoxic.gm_kub.entity.MiniGame;
import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@ApplicationScoped
public class MiniGameRepository extends BaseRepository {

    @Transactional
    public MiniGame find(int id) {
        return (MiniGame)super.find(MiniGame.class, id);
    }

    @Transactional
    public List<MiniGame> findAll() {
        return em.createNamedQuery("MiniGame.findAll", MiniGame.class).getResultList();
    }

    @Transactional
    public MiniGame findByName(String name) {
        try {
            return em.createNamedQuery("MiniGame.findByName", MiniGame.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch(Exception exception) {
            return null;
        }
    }
}
