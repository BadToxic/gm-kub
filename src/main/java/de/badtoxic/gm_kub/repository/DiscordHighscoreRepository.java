package de.badtoxic.gm_kub.repository;

import de.badtoxic.gm_kub.discord.events.EndDiscordGame;
import de.badtoxic.gm_kub.entity.DiscordHighscore;
import de.badtoxic.gm_kub.type.DiscordGameEnum;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.ObservesAsync;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@Transactional(Transactional.TxType.REQUIRED)
@ApplicationScoped
public class DiscordHighscoreRepository extends BaseRepository {

    public DiscordHighscore find(int id) {
        return (DiscordHighscore)super.find(DiscordHighscore.class, id);
    }

    public List<DiscordHighscore> findAll() {
        return em.createNamedQuery("DiscordHighscore.findAll", DiscordHighscore.class).getResultList();
    }

    public List<DiscordHighscore> findByDiscordUserId(String discordUserID) {
        try {
            return em.createNamedQuery("DiscordHighscore.findByDiscordUserId", DiscordHighscore.class)
                    .setParameter("discordUserID", discordUserID)
                    .getResultList();
        } catch(Exception exception) {
            System.out.println("findByDiscordUserId: " + exception);
            return null;
        }
    }

    public List<DiscordHighscore> findByDiscordGameName(DiscordGameEnum discordGameName) {
        try {
            return em.createNamedQuery("DiscordHighscore.findByDiscordGameName", DiscordHighscore.class)
                    .setParameter("discordGameName", discordGameName)
                    .getResultList();
        } catch(Exception exception) {
            System.out.println("findByDiscordGameName: " + exception);
            return null;
        }
    }

    public DiscordHighscore findByDiscordUserIdAndGameName(String discordUserID, DiscordGameEnum discordGameName) {
        try {
            return em.createNamedQuery("DiscordHighscore.findByDiscordUserIdAndGameName", DiscordHighscore.class)
                    .setParameter("discordUserID", discordUserID)
                    .setParameter("discordGameName", discordGameName)
                    .getSingleResult();
        } catch(Exception exception) {
            System.out.println("findByDiscordUserIdAndGameName: " + exception);
            return null;
        }
    }

    public void saveOrUpdateHighscore(DiscordHighscore highscore){
        try {
            DiscordHighscore oldHighscore
                = findByDiscordUserIdAndGameName(highscore.getDiscordUserID(), highscore.getDiscordGameName());
            if (oldHighscore == null) {
                em.persist(highscore);
                em.flush();
                System.out.println("Persisted highscore: " + highscore);
            } else if (highscore.getScore() > oldHighscore.getScore()) {
                oldHighscore.setScore(highscore.getScore());
                em.merge(oldHighscore);
                em.flush();
            }
        } catch (Exception exception) {
            System.out.println(exception);
        }
    }

    public void onGameEnded(@ObservesAsync @EndDiscordGame DiscordHighscore highscore){
        saveOrUpdateHighscore(highscore);
    }
}
