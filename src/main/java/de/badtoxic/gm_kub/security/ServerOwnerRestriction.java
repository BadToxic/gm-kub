package de.badtoxic.gm_kub.security;

import javax.interceptor.InterceptorBinding;
import java.lang.annotation.*;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@Inherited
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface ServerOwnerRestriction {
}

