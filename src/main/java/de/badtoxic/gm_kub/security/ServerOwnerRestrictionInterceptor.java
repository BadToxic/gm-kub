package de.badtoxic.gm_kub.security;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import javax.annotation.Priority;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@Interceptor
@ServerOwnerRestriction
@Priority(Interceptor.Priority.APPLICATION)
public class ServerOwnerRestrictionInterceptor {

    @AroundInvoke
    public Object mustOwnServer(InvocationContext ctx) throws Exception {
        Object[] parameters = ctx.getParameters();
        MessageReceivedEvent event = (MessageReceivedEvent) parameters[0];
        String channelID = (String) parameters[1];
        TextChannel textChannel = event.getJDA().getTextChannelById(channelID);
        Member serverOwner = textChannel.getGuild().getOwner();
        if (!serverOwner.getUser().equals(event.getAuthor())) {
            event.getChannel().sendMessage("Only the server owner **" + serverOwner.getEffectiveName() +
                    "** is allowed to use this command.").queue();
            return null;
        }
        return ctx.proceed();
    }
}
