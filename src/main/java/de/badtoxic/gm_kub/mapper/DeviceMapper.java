package de.badtoxic.gm_kub.mapper;

import de.badtoxic.gm_kub.dto.DeviceDTO;
import de.badtoxic.gm_kub.entity.Device;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@Mapper
public interface DeviceMapper {
    DeviceMapper INSTANCE = Mappers.getMapper(DeviceMapper.class);

    DeviceDTO toDTO(Device miniGame);
}
