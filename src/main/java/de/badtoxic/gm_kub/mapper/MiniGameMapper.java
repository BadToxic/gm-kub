package de.badtoxic.gm_kub.mapper;

import de.badtoxic.gm_kub.dto.MiniGameDTO;
import de.badtoxic.gm_kub.entity.MiniGame;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@Mapper
public interface MiniGameMapper {
    MiniGameMapper INSTANCE = Mappers.getMapper(MiniGameMapper.class);

//    @Mapping(source = "numberOfSeats", target = "seatCount")
    MiniGameDTO toDTO(MiniGame miniGame);

}
