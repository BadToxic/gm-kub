package de.badtoxic.gm_kub.mapper;

import de.badtoxic.gm_kub.dto.ItemDTO;
import de.badtoxic.gm_kub.entity.Item;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@Mapper
public interface ItemMapper {
    ItemMapper INSTANCE = Mappers.getMapper(ItemMapper.class);

    ItemDTO toDTO(Item miniGame);
}
