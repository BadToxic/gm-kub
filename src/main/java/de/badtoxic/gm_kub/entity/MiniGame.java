package de.badtoxic.gm_kub.entity;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
        @NamedQuery(name="MiniGame.findAll",
                query="SELECT mg FROM MiniGame mg"),
        @NamedQuery(name="MiniGame.findByName",
                query="SELECT mg FROM MiniGame mg WHERE mg.name = :name"),
})
@Table(name = "mini_games")
public class MiniGame extends BaseEntity {

    public MiniGame() {}

    public MiniGame(int id, String name, String image) {
        super(id, name, image);
    }
}
