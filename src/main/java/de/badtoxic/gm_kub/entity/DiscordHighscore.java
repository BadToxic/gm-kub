package de.badtoxic.gm_kub.entity;

import de.badtoxic.gm_kub.type.DiscordGameEnum;

import javax.persistence.*;

@Entity
@Table(name = "discord_highscores")
@NamedQueries({
    @NamedQuery(name="DiscordHighscore.findAll",
        query="SELECT highscore FROM DiscordHighscore highscore"),
    @NamedQuery(name="DiscordHighscore.findByDiscordUserId",
        query="SELECT highscore FROM DiscordHighscore highscore WHERE highscore.discordUserID = :discordUserID"),
    @NamedQuery(name="DiscordHighscore.findByDiscordGameName",
        query="SELECT highscore FROM DiscordHighscore highscore WHERE highscore.discordGameName = :discordGameName " +
                                                               "ORDER BY highscore.score DESC"),
    @NamedQuery(name="DiscordHighscore.findByDiscordUserIdAndGameName",
        query="SELECT highscore FROM DiscordHighscore highscore WHERE highscore.discordUserID = :discordUserID AND " +
                                                                     "highscore.discordGameName = :discordGameName"),
})
public class DiscordHighscore {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    public int id;

    @Column(name = "discord_game_name", columnDefinition = "VARCHAR2(32)")
    public DiscordGameEnum discordGameName;

    @Column(name = "discord_user_id", columnDefinition = "VARCHAR2(32)")
    public String discordUserID;

    @Column(name = "discord_user_name", columnDefinition = "VARCHAR2(32)")
    public String discordUserName;

    @Column(name = "score")
    public Long score;

    public DiscordHighscore() {}

    public DiscordHighscore(int id, DiscordGameEnum discordGameName,
                            String discordUserID, String discordUserName, Long score) {
        this.id = id;
        this.discordGameName = discordGameName;
        this.discordUserID = discordUserID;
        this.discordUserName = discordUserName;
        this.score = score;
    }
    public DiscordHighscore(DiscordGameEnum discordGameName, String discordUserID, String discordUserName, Long score) {
        this.discordGameName = discordGameName;
        this.discordUserID = discordUserID;
        this.discordUserName = discordUserName;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DiscordGameEnum getDiscordGameName() {
        return discordGameName;
    }

    public void setDiscordGameName(DiscordGameEnum discordGameName) {
        this.discordGameName = discordGameName;
    }

    public String getDiscordUserID() {
        return discordUserID;
    }

    public void setDiscordUserID(String discordUserID) {
        this.discordUserID = discordUserID;
    }

    public String getDiscordUserName() {
        return discordUserName;
    }

    public void setDiscordUserName(String discordUserName) {
        this.discordUserName = discordUserName;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "(id: " + id + ", game: " + discordGameName + ", user: " + discordUserID + ", score: " + score + ")";
    }
}
