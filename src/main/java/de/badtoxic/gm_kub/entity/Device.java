package de.badtoxic.gm_kub.entity;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
        @NamedQuery(name="Device.findAll",
                query="SELECT device FROM Device device"),
        @NamedQuery(name="Device.findByName",
                query="SELECT device FROM Device device WHERE device.name = :name"),
})
@Table(name = "devices")
public class Device extends BaseEntity {

    public Device() {}

    public Device(int id, String name, String image) {
        super(id, name, image);
    }
}
