package de.badtoxic.gm_kub.entity;

import de.badtoxic.gm_kub.type.ConsoleTypeEnum;
import de.badtoxic.gm_kub.type.DataHolderTypeEnum;
import de.badtoxic.gm_kub.type.ItemTypeEnum;

import javax.persistence.*;

@Entity
@Table(name = "items")
@NamedQueries({
        @NamedQuery(name="Item.findAll",
                query="SELECT item FROM Item item"),
        @NamedQuery(name="Item.findByName",
                query="SELECT item FROM Item item WHERE item.name = :name"),
})
public class Item extends BaseEntity {

    @Column(name = "description", columnDefinition = "VARCHAR2(256)")
    public String description;

    @Column(name = "item_type", columnDefinition = "VARCHAR2(32)", nullable = false)
    @Enumerated(EnumType.STRING)
    public ItemTypeEnum itemType;

    @Column(name = "console_type", columnDefinition = "VARCHAR2(32)", nullable = true)
    @Enumerated(EnumType.STRING)
    public ConsoleTypeEnum consoleType;

    @Column(name = "data_holder_type", columnDefinition = "VARCHAR2(32)", nullable = true)
    @Enumerated(EnumType.STRING)
    public DataHolderTypeEnum dataHolderType;

    public Item() {}

    public Item(int id, String name, String image, String description) {
        super(id, name, image);
        this.setDescription(description);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemTypeEnum getItemType() {
        return itemType;
    }

    public void setItemType(ItemTypeEnum itemType) {
        this.itemType = itemType;
    }

    public ConsoleTypeEnum getConsoleType() {
        return consoleType;
    }

    public void setConsoleType(ConsoleTypeEnum consoleType) {
        this.consoleType = consoleType;
    }

    public DataHolderTypeEnum getDataHolderType() {
        return dataHolderType;
    }

    public void setDataHolderType(DataHolderTypeEnum dataHolderType) {
        this.dataHolderType = dataHolderType;
    }
}
