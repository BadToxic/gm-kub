package de.badtoxic.gm_kub.entity;

import javax.persistence.*;

@MappedSuperclass
public class BaseEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    public int id;

    @Column(name = "name", columnDefinition = "VARCHAR2(32)")
    public String name;

    @Column(name = "image", columnDefinition = "VARCHAR2(32)")
    public String image;

    public BaseEntity() {}

    public BaseEntity(int id, String name, String image) {
        this.setId(id);
        this.setName(name);
        this.setImage(image);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
