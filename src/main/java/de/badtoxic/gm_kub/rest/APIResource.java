package de.badtoxic.gm_kub.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;

/**
 * REST Resource entry point
 * 
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@RequestScoped
@Path("/api")
public class APIResource {

    private GraphQLResource graphQLResource;
    private HelloWorldResource helloWorldResource;

    public APIResource() {}

    @Inject
    public APIResource(GraphQLResource graphQLResource, HelloWorldResource helloWorldResource) {
        this.graphQLResource = graphQLResource;
        this.helloWorldResource = helloWorldResource;
    }

    @Path("graphql")
    public GraphQLResource getGraphQLResource() {
        return graphQLResource;
    }

    @Path("hello")
    public HelloWorldResource getHelloWorldResource() {
        return helloWorldResource;
    }
}
