package de.badtoxic.gm_kub.rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
/**
 * REST Resource for simple testing
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@RequestScoped
public class HelloWorldResource {

    @GET
    @Produces("text/plain")
    public Response doGet() {
        return Response.ok("Hello from Thorntail!").build();
    }
}
