package de.badtoxic.gm_kub.rest.filter;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@Provider
public class LogFilter implements ContainerRequestFilter, ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext reqContext) throws IOException {
        System.out.println("-- REST Request on " + reqContext.getUriInfo().getPath());
//        log(reqContext.getUriInfo(), reqContext.getHeaders());
    }

    @Override
    public void filter(ContainerRequestContext reqContext,
                       ContainerResponseContext resContext) throws IOException {
        System.out.println("-- REST Response delivered for: " + reqContext.getUriInfo().getPath());
//        log(reqContext.getUriInfo(), resContext.getHeaders());
    }

//    private void log(UriInfo uriInfo, MultivaluedMap<String, ?> headers) {
//        System.out.println("Path: " + uriInfo.getPath());
//        headers.entrySet().forEach(h -> System.out.println(h.getKey() + ": " + h.getValue()));
//    }
}