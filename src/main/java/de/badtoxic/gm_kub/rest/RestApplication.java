package de.badtoxic.gm_kub.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * REST Resource entry point
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@ApplicationPath("/")
public class RestApplication extends Application {}