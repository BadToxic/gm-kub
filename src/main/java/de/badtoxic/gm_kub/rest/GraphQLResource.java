package de.badtoxic.gm_kub.rest;

import de.badtoxic.gm_kub.graphql.GraphQLEndPoint;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@RequestScoped
public class GraphQLResource {

    private GraphQLEndPoint graphQLEndPoint;

    public GraphQLResource() {}

    @Inject
    public GraphQLResource(GraphQLEndPoint graphQLEndPoint) {
        this.graphQLEndPoint = graphQLEndPoint;
    }

    @POST
//    @Operation(summary = "Search In ECP Release catalogue, with a plain graphql query.", description = "Takes GraphQL query as plain text and returns" +
//            " results. See schema for details.")
//    @ApiResponse(description = "Response based on your graphql query.")
    @Consumes({"application/graphql", "text/plain"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response search(String graphQLQuery) {
        return graphQLEndPoint.search(graphQLQuery);
    }
}
