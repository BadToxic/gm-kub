package de.badtoxic.gm_kub.dto;

/**
 * DTO for devices
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class DeviceDTO extends BaseDTO {}
