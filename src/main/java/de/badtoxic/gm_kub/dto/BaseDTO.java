package de.badtoxic.gm_kub.dto;

/**
 * DTO base object
 * 
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class BaseDTO {

    public int id;
    public String name;
    public String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
