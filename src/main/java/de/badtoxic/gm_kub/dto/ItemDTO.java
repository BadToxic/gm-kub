package de.badtoxic.gm_kub.dto;

import de.badtoxic.gm_kub.type.ConsoleTypeEnum;
import de.badtoxic.gm_kub.type.DataHolderTypeEnum;
import de.badtoxic.gm_kub.type.ItemTypeEnum;

/**
 * DTO for items
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class ItemDTO extends BaseDTO {

    public String description;
    public ItemTypeEnum itemType;
    public ConsoleTypeEnum consoleType;
    public DataHolderTypeEnum dataHolderType;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemTypeEnum getItemType() {
        return itemType;
    }

    public void setItemType(ItemTypeEnum itemType) {
        this.itemType = itemType;
    }

    public ConsoleTypeEnum getConsoleType() {
        return consoleType;
    }

    public void setConsoleType(ConsoleTypeEnum consoleType) {
        this.consoleType = consoleType;
    }

    public DataHolderTypeEnum getDataHolderType() {
        return dataHolderType;
    }

    public void setDataHolderType(DataHolderTypeEnum dataHolderType) {
        this.dataHolderType = dataHolderType;
    }
}
