package de.badtoxic.gm_kub.dto;

/**
 * DTO for mini games
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class MiniGameDTO extends BaseDTO {}
