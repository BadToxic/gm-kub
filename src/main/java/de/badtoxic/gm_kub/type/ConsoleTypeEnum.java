package de.badtoxic.gm_kub.type;

/**
 * Consoles in my unity game GameMaster
 * 
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public enum ConsoleTypeEnum {
    GameGuy,
    Tamagotchi,
    Pairs,
    GameOfLife,
    GameOfFifteen
}
