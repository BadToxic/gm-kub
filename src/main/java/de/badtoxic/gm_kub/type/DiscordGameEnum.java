package de.badtoxic.gm_kub.type;

/**
 * Discord game enum
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public enum DiscordGameEnum {
    SNAKE("snake"),
    PRESIDENT("president");

    private final String text;

    DiscordGameEnum(final String text) {
        this.text = text;
    }

    public static DiscordGameEnum fromString(String text) {
        for (DiscordGameEnum gameEnum : DiscordGameEnum.values()) {
            if (gameEnum.text.equalsIgnoreCase(text)) {
                return gameEnum;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }
}
