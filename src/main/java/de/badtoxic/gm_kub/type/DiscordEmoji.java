package de.badtoxic.gm_kub.type;

/**
 * Enums representing Emojis available in Discord.
 * Mainly use Unicode instead of discord codes. Eg. from https://www.fileformat.info/info/emoji/list.htm
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public enum DiscordEmoji {
    ArrowUp("U+2b06U+fe0f"),
    ArrowRight("U+27a1U+fe0f"),
    ArrowDown("U+2b07U+fe0f"),
    ArrowLeft("U+2b05U+fe0f"),
    ArrowCircle("U+1f504"),
    CheckMarkGreen("U+2705"),
    OK("U+1F197"),

    Num0("U+30U+20E3", "zero"),
    Num1("U+31U+20E3", "one"),
    Num2("U+32U+20E3", "two", 13),
    Num3("U+33U+20E3", "three", 1),
    Num4("U+34U+20E3", "four", 2),
    Num5("U+35U+20E3", "five", 3),
    Num6("U+36U+20E3", "six", 4),
    Num7("U+37U+20E3", "seven", 5),
    Num8("U+38U+20E3", "eight", 6),
    Num9("U+39U+20E3", "nine", 7),
    Num10("U+1F51F", "keycap_ten", 8),
    // BoyTuxedo("U+1F935", "man_in_tuxedo", 9),
    // Princess("U+1F478", "princess", 10),
    // Prince("U+1F934", "prince", 11),
    LetterA("U+1F1E6", "regional_indicator_a", 12),
    LetterJ("U+1F1EF", "regional_indicator_j", 9),
    LetterK("U+1F1F0", "regional_indicator_k", 11),
    LetterQ("U+1F1F6", "regional_indicator_q", 10),
    Joker("U+1F0CF", "black_joker", 100),

    // ThumbsUp("U+1F44D", "thumbsup"),
    // FingersV("U+270C", "v"),
    // LoveYouGesture("U+1F91F", "love_you_gesture"),

    TrackNext("U+23EDU+FE0F", "track_next"),
    Pause("U+23f8U+fe0f"),
    PausePlay("U+23efU+fe0f"),
    RedX("U+274c");

    private final String text;
    private final String code;
    private final int presidentValue; // Value of a card emoji for the game President

    DiscordEmoji(final String text) {
        this.text = text;
        code = null;
        presidentValue = 0;
    }
    DiscordEmoji(final String text, final String code) {
        this.text = text;
        this.code = code;
        presidentValue = 0;
    }
    DiscordEmoji(final String text, final String code, int presidentValue) {
        this.text = text;
        this.code = code;
        this.presidentValue = presidentValue;
    }

    public static DiscordEmoji fromString(String text) {
        for (DiscordEmoji emojiEnum : DiscordEmoji.values()) {
            if (emojiEnum.text.equalsIgnoreCase(text)) {
                return emojiEnum;
            }
        }
        return null;
    }
    public static DiscordEmoji fromPresidentValue(int value) {
        for (DiscordEmoji emojiEnum : DiscordEmoji.values()) {
            if (emojiEnum.presidentValue == value) {
                return emojiEnum;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }
    public String toCode() {
        return ':' + code + ':';
    }
    public int toPresidentValue() {
        return presidentValue;
    }
}
