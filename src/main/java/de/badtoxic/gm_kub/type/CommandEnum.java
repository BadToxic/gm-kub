package de.badtoxic.gm_kub.type;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public enum CommandEnum {
    HELP("help"),
    INVITE("invite"),
    REPEAT("repeat"),
    START("start");

    private final String text;

    CommandEnum(final String text) {
        this.text = text;
    }

    public static CommandEnum fromString(String text) {
        for (CommandEnum commandEnum : CommandEnum.values()) {
            if (commandEnum.text.equalsIgnoreCase(text)) {
                return commandEnum;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }
}
