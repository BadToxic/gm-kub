package de.badtoxic.gm_kub.type;

/**
 * Entities we can handle
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public enum EntityTypeEnum {
    MiniGame("game"),
    Device("device"),
    Item("item");

    private final String text;

    EntityTypeEnum(final String text) {
        this.text = text;
    }

    public static EntityTypeEnum fromString(String text) {
        for (EntityTypeEnum entityTypeEnum : EntityTypeEnum.values()) {
            if (entityTypeEnum.text.equalsIgnoreCase(text)) {
                return entityTypeEnum;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }
}
