package de.badtoxic.gm_kub.type;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public enum DirectionEnum {
    Up,
    Right,
    Down,
    Left
}
