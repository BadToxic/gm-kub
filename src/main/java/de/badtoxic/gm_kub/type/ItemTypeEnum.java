package de.badtoxic.gm_kub.type;

/**
 * Items in my unity game GameMaster
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public enum ItemTypeEnum {
    Console,
    DataHolder,
    Storage,
    Battery,
    Clock
}
