package de.badtoxic.gm_kub.type;

/**
 * Data Holders in my unity game GameMaster
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public enum DataHolderTypeEnum {
    GameGuyCartridge,
    GameCard
}
