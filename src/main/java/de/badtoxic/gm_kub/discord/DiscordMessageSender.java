package de.badtoxic.gm_kub.discord;

import de.badtoxic.gm_kub.security.ServerOwnerRestriction;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

import static de.badtoxic.gm_kub.discord.DiscordMessageBuilder.*;
import static de.badtoxic.gm_kub.type.CommandEnum.REPEAT;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@ApplicationScoped
public class DiscordMessageSender {

    @Inject
    @ConfigurationValue("discord.command")
    String botCommand;

    public void sendMissingCommand(MessageReceivedEvent event) {
        event.getChannel().sendMessage("You need to add a command.").queue(
            response -> {
                response.editMessage(buildHowToGetHelpMessage()).queue();
            }
        );
    }
    public void sendUnknownCommand(MessageReceivedEvent event, String command) {
        if(command.length() > 20) {
            command = command.substring(0, 18) + "...";
        }
        event.getChannel().sendMessage("The command " + command + " is unknown.").queue(
            response -> {
                response.editMessage(buildHowToGetHelpMessage()).queue();
            }
        );
    }
    public void sendHelp(MessageReceivedEvent event) {
        event.getChannel().sendMessage("I know the following commands:").queue(
            response -> {
                response.editMessage(buildHelpMessage()).queue();
            }
        );
    }
    public void sendInvite(GuildMessageReceivedEvent event) {
        TextChannel textChannel = event.getChannel();
        GuildChannel guildChannel = textChannel;
        Guild guild = event.getGuild();
        Invite invite = null;
        try {
            List<Invite> invites = guild.retrieveInvites().complete();
            Invite inviteRetrieved = invites.get(0);
            if (!inviteRetrieved.isTemporary() && !inviteRetrieved.toString().equals(null)) {
                invite = inviteRetrieved;
            }
        } catch (InsufficientPermissionException exc) {
            System.out.printf("Trying guild.retrieveInvites: " + exc.getMessage());
        }
        if (invite == null) {
            try {
                invite = guildChannel.createInvite().setTemporary(false).complete();
            } catch (InsufficientPermissionException exc) {
                textChannel.sendMessage(exc.getMessage()).queue();
                return;
            }
        }
        textChannel.sendMessage(DiscordMessageBuilder.buildInvitationMessage(guild, invite)).queue();
    }

    public void sendPong(MessageChannel channel) {
        long time = System.currentTimeMillis();
        channel.sendMessage("Pong!").queue(response -> response.editMessageFormat("Pong: %d ms",
                System.currentTimeMillis() - time).queue());
    }

    @ServerOwnerRestriction
    public void sendRepeat(MessageReceivedEvent event, String channelID) {
        TextChannel textChannel = event.getJDA().getTextChannelById(channelID);
        String text = event.getMessage().getContentRaw().replace(botCommand + " " + REPEAT + " " + channelID + " ", "");
        textChannel.sendMessage(text).queue();
    }
}
