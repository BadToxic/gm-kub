package de.badtoxic.gm_kub.discord.game;

import de.badtoxic.gm_kub.type.DirectionEnum;
import de.badtoxic.gm_kub.type.DiscordEmoji;
import de.badtoxic.gm_kub.type.DiscordGameEnum;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.restaction.MessageAction;

import javax.annotation.Nullable;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static de.badtoxic.gm_kub.discord.DiscordMessageBuilder.buildHowToGetHelpMessage;
import static de.badtoxic.gm_kub.type.DiscordEmoji.*;

/**
 * Abstract base class for a Discord Game
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public abstract class DiscordGame {

    protected static final Map<DirectionEnum, String> directionToEmoji = new HashMap<>(){{
        put(DirectionEnum.Up, ":arrow_up:");
        put(DirectionEnum.Right, ":arrow_right:");
        put(DirectionEnum.Down, ":arrow_down:");
        put(DirectionEnum.Left, ":arrow_left:");
    }};
    protected DiscordGameHandler gameHandler;
    protected DiscordGameEnum discordGameName;
    protected User creator;
    protected Message message;
    protected boolean pause = false;
    protected boolean pauseBlink = false;
    protected boolean gameOver = false;
    protected List<DiscordEmoji> neededReactions; // All the time
    protected List<DiscordEmoji> neededReactionsBeforePreparation;
    protected List<DiscordEmoji> neededReactionsAfterPreparation;
    protected TimeUnit stepTimeUnit = TimeUnit.SECONDS;
    protected int stepDuration = 1;
    protected String errors;
    protected long score;
    protected boolean onlyCreatorReactions = true;

    protected DiscordGame(DiscordGameHandler gameHandler, User creator) {
        this.gameHandler = gameHandler;
        this.creator = creator;
        neededReactions = new ArrayList<>();
    }

    protected void startBase() {
        pause = false;
        pauseBlink = false;
        gameOver = false;
    }

    public void pause() {
        if (gameOver) {
            return;
        }
        pause = !pause;
    }
    public void stop(boolean destroyImmediately) {
        // If it doesn't run automatically, trigger the next and last step
        if (!gameOver && stepDuration <= 0) {
            gameOver = true;
            pause = false;
            if (!destroyImmediately) {
                System.out.println("Stop game: one more iteration");
                run(this.message);
            } else {
                gameHandler.onGameStopped(message);
            }
            return;
        }

        gameOver = true;
        pause = false;
        if (destroyImmediately) {
            gameHandler.onGameStopped(message);
        }
    }
    public void restart() {
        if (gameOver) {
            return;
        }
        start();
    }

    protected abstract void start();
    protected abstract boolean nextStep();
    public abstract void setDirection(DirectionEnum direction);
    public abstract void handleReaction(DiscordEmoji emoji, String userId, String userName, @Nullable User user);

    public void run(Message message) {
        if (pause) {
            if (pauseBlink) {
                message.removeReaction(Pause.toString())
                    .queueAfter(stepDuration, stepTimeUnit, response -> run(message));
            } else {
                message.addReaction(Pause.toString())
                    .queueAfter(stepDuration, stepTimeUnit, response -> run(message));
            }
            pauseBlink = !pauseBlink;
            return;
        }
        if (pauseBlink) {
            message.removeReaction(Pause.toString()).queue();
            pauseBlink = false;
        }
        if (!gameOver && nextStep()) {
            MessageAction messageAction = message.editMessage(toString());
            if (stepDuration > 0) {
                messageAction.queueAfter(stepDuration, stepTimeUnit, response -> run(response));
            } else {
                messageAction.queue();
            }
            return;
        }
        stop(false);
        message.editMessage(toString()).queue();
        // Remove all reactions
        if (pauseBlink) {
            message.removeReaction(Pause.toString()).queue();
        }
        neededReactions.forEach(emoji -> message.removeReaction(emoji.toString()).queue());
        if (neededReactionsBeforePreparation != null) {
            neededReactionsBeforePreparation.forEach(emoji -> message.removeReaction(emoji.toString()).queue());
        }
        if (neededReactionsAfterPreparation != null) {
            neededReactionsAfterPreparation.forEach(emoji -> message.removeReaction(emoji.toString()).queue());
        }
        gameHandler.onGameStopped(message);
    }

    public String init(Message message) {
        this.message = message;
        neededReactions.forEach(emoji -> message.addReaction(emoji.toString()).queue());
        if (neededReactionsBeforePreparation != null) {
            neededReactionsBeforePreparation.forEach(emoji -> message.addReaction(emoji.toString()).queue());
        }
        if (errors != null && !errors.isEmpty()) {
            message.editMessage(new EmbedBuilder().setColor(Color.red)
                    .setTitle("*The following errors occurred:*")
                    .setDescription(errors)
                    .build()).queue();
        }
        run(message);
        return message.getId();
    }
    // This is optional and only needed by games that need to prepare the game
    protected void finishedPreparations() {
        if (neededReactionsBeforePreparation != null) {
            neededReactionsBeforePreparation.forEach(emoji -> message.removeReaction(emoji.toString()).queue());
        }
        if (neededReactionsAfterPreparation != null) {
            neededReactionsAfterPreparation.forEach(emoji -> message.addReaction(emoji.toString()).queue());
        }
    }

    public User getCreator() {
        return creator;
    }

    public long getScore() {
        return score;
    }

    public DiscordGameEnum getDiscordGameName() {
        return discordGameName;
    }

    public boolean isOnlyCreatorReactions() {
        return onlyCreatorReactions;
    }
}
