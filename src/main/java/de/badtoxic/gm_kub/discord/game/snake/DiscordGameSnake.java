package de.badtoxic.gm_kub.discord.game.snake;

import de.badtoxic.gm_kub.discord.game.DiscordGame;
import de.badtoxic.gm_kub.discord.game.DiscordGameHandler;
import de.badtoxic.gm_kub.type.DirectionEnum;
import de.badtoxic.gm_kub.type.DiscordEmoji;
import de.badtoxic.gm_kub.type.DiscordGameEnum;
import net.dv8tion.jda.api.entities.User;
import org.antlr.v4.runtime.misc.Pair;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static de.badtoxic.gm_kub.type.DiscordEmoji.*;
import static java.awt.Color.yellow;

/**
 * Snake Game
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class DiscordGameSnake extends DiscordGame {

    private static final int startBodyLength = 5;
    private static final int appleAfterSteps = 4;
    private static final int widthDefault = 8;
    private static final int heightDefault = 8;
    private static final int sizeMin = 6;
    private static final int sizeMax = 16;
    private static final int stepDurationMin = 750; // Milliseconds
    private static final int stepDurationMax = 1500;
    private static final int sizeMaxWithEmojis = 9;
    private Map<String, String> colorToBodyEmoji = new HashMap<>(){{
        put("yellow", ":yellow_circle:");
        put("orange", ":orange_circle:");
        put("brown", ":brown_circle:");
        put("blue", ":blue_circle:");
        put("white", ":white_circle:");
        put("grey", ":new_moon:");
        put("red", ":red_circle:");
        put("purple", ":purple_circle:");
        put("green", ":green_circle:");
    }};
    private Map<String, String> colorToHeadEmoji = new HashMap<>(){{
        put("yellow", ":slight_smile:");
        put("orange", ":jack_o_lantern:");
        put("brown", ":poop:");
        put("blue", ":cold_face:");
        put("white", ":alien:");
        put("grey", ":new_moon_with_face:");
        put("red", ":rage:");
        put("purple", ":smiling_imp:");
        put("green", ":nauseated_face:");
    }};
    private static final String emptyEmoji = ":black_medium_square:"; // This has 21 chars
    private static final String appleEmoji = ":apple:";
    private static final String emptyText = "⠀";
    private static final String bodyText = "X";
    private static final String headText = "0";
    private static final String appleText = "A";

    private String[][] gameArea;
    private boolean isEmojiMode;
    private String empty; // This has 21 chars
    private String body;
    private String head;
    private String apple;
    private String color;
    private int width;
    private int height;
    private int headX;
    private int headY;
    private int appleX;
    private int appleY;
    private boolean isApplePresent;
    private DirectionEnum direction;
    private List<DirectionEnum> directions; // Next directions
    private List<Pair<Integer, Integer>> tail;
    private int stepsTillApple;

    public DiscordGameSnake(DiscordGameHandler gameHandler, User creator, String arguments) {
        super(gameHandler, creator);
        discordGameName = DiscordGameEnum.SNAKE;
        isEmojiMode = true;
        this.width = widthDefault;
        this.height = heightDefault;
        neededReactions.add(ArrowLeft);
        neededReactions.add(ArrowUp);
        neededReactions.add(ArrowRight);
        neededReactions.add(ArrowDown);
        neededReactions.add(ArrowCircle);
        neededReactions.add(PausePlay);
        neededReactions.add(RedX);
        tail = new ArrayList<>();
        color = colorToBodyEmoji.keySet().stream().skip((int) (colorToBodyEmoji.keySet().size() * Math.random()))
                .findFirst().get();
        handleArguments(arguments);
        empty = isEmojiMode ? emptyEmoji : emptyText;
        body = isEmojiMode ? colorToBodyEmoji.get(color) : bodyText;
        head = isEmojiMode ? colorToHeadEmoji.get(color) : headText;
        apple = isEmojiMode ? appleEmoji : appleText;
        directions = new ArrayList<>();
        start();
    }

    private void handleArguments(String arguments) {
        String[] msgs = arguments.split(" ");
        errors = "";
        for (int msgIndex = 0; msgIndex < msgs.length; msgIndex++) {
            String msg = msgs[msgIndex];
            switch (msg) {
                case "text": {
                    isEmojiMode = false;
                    break;
                }
                case "time": {
                    if (msgs.length < msgIndex + 2) {
                        errors += "Parameter missing to set the game step duration in milliseconds. Example: time 850\n";
                        continue;
                    }
                    String numberError = "Parameter(s) to set the game step duration in milliseconds " +
                            "must be numbers between [" + stepDurationMin + ", " + stepDurationMax + "].\n";
                    try {
                        stepDuration = Integer.parseInt(msgs[msgIndex + 1]);
                        stepTimeUnit = TimeUnit.MILLISECONDS;
                    } catch (NumberFormatException exception) {
                        errors += numberError;
                        continue;
                    }
                    msgIndex++;
                    if (stepDuration < stepDurationMin || stepDuration > stepDurationMax) {
                        errors += numberError;
                        // Reset to default
                        stepTimeUnit = TimeUnit.SECONDS;
                        stepDuration = 1;
                    }
                    break;
                }
                case "size": {
                    if (msgs.length < msgIndex + 3) {
                        errors += "Parameter(s) missing to set the game area size. Example: size 10 8\n";
                        continue;
                    }
                    String numberError = "Parameter(s) to set the game area size " +
                            "must be numbers between [" + sizeMin + ", " + sizeMaxWithEmojis + "] in emoji mode," +
                            "or between [" + sizeMin + ", " + sizeMax + "] in text mode.\n";
                    try {
                        width = Integer.parseInt(msgs[msgIndex + 1]);
                        height = Integer.parseInt(msgs[msgIndex + 2]);
                    } catch (NumberFormatException exception) {
                        errors += numberError;
                        continue;
                    }
                    msgIndex += 2;
                    if (width < sizeMin || height < sizeMin
                            || (isEmojiMode && (width > sizeMaxWithEmojis || height > sizeMaxWithEmojis))
                            || width > sizeMax || height > sizeMax) {
                        errors += numberError;
                        // Reset to default
                        width = widthDefault;
                        height = heightDefault;
                    }
                    break;
                }
                case "color": {
                    if (msgs.length < msgIndex + 2) {
                        errors += "Parameter missing to set the snake color. Example: color blue\n";
                        continue;
                    }
                    String colorToSet = msgs[msgIndex + 1];
                    if (!colorToBodyEmoji.keySet().contains(colorToSet)) {
                        errors +=  "Parameter to set snake color must be one of:\n" +
                                String.join(", ", colorToBodyEmoji.keySet());
                    } else {
                        color = colorToSet;
                    }
                    break;
                }
            }
        }
    }

    @Override
    protected void start() {
        startBase();
        gameArea = new String[width][height];
        for (int xIndex = 0; xIndex < width; xIndex++) {
            for (int yIndex = 0; yIndex < height; yIndex++) {
                gameArea[xIndex][yIndex] = empty;
            }
        }
        setHead(0, height / 2);
        tail.clear();
        for (int bodyPart = 1; bodyPart < startBodyLength; bodyPart++) {
            tail.add(new Pair(headX, headY));
        }
        direction = DirectionEnum.Right;
        directions.clear();
        isApplePresent = false;
        stepsTillApple = appleAfterSteps;
        score = 0;
    }

    public void setDirection(DirectionEnum direction) {
        DirectionEnum directionPrev = this.direction;
        if (!directions.isEmpty()) {
            directionPrev = directions.get(directions.size() - 1);
        }
        if (!isDirectionForbidden(direction, directionPrev)) {
            directions.add(direction);
        }
    }

    @Override
    public void handleReaction(DiscordEmoji emoji, String userId, String userName, @Nullable User user) {
        // No special case in this game
    }

    private boolean isDirectionForbidden(DirectionEnum direction, DirectionEnum directionPrev) {
        return DirectionEnum.Up.equals(direction) && DirectionEnum.Down.equals(directionPrev)
                || DirectionEnum.Right.equals(direction) && DirectionEnum.Left.equals(directionPrev)
                || DirectionEnum.Down.equals(direction) && DirectionEnum.Up.equals(directionPrev)
                || DirectionEnum.Left.equals(direction) && DirectionEnum.Right.equals(directionPrev);
    }

    @Override
    protected boolean nextStep() {
        int x = headX, y = headY;

        if (!directions.isEmpty()) {
            direction = directions.remove(0);
        }

        switch (direction) {
            case Up: {
                if (headY <= 0) {
                    return false;
                }
                y--;
                break;
            }
            case Right: {
                if (headX >= width - 1) {
                    return false;
                }
                x++;
                break;
            }
            case Down: {
                if (headY >= height - 1) {
                    return false;
                }
                y++;
                break;
            }
            case Left: {
                if (headX <= 0) {
                    return false;
                }
                x--;
                break;
            }
        }

        Pair<Integer, Integer> lastPart = tail.remove(0);

        // Check if head collides with any body part (of new step)
        int finalX = x, finalY = y;
        if (tail.stream().anyMatch(part -> part.a.equals(finalX) && part.b.equals(finalY))) {
            return false;
        }

        // Add new part underneath the head
        tail.add(new Pair<>(headX, headY));
        if (isApplePresent) {
            if (appleX == x && appleY == y) {
                score++;
                isApplePresent = false;
                tail.add(0, lastPart); // Re-add the last part
            }
        }

        // Clear old field, if no other body part blocks it
        if (!tail.stream().anyMatch(part -> part.a.equals(lastPart.a) && part.b.equals(lastPart.b))) {
            setField(lastPart.a, lastPart.b, empty);
        }
        setField(headX, headY, body);

        setHead(x, y);

        if (!isApplePresent) {
            // Create an apple
            stepsTillApple--;
            if (stepsTillApple == 0) {
                stepsTillApple = appleAfterSteps;
                do {
                    appleX = ThreadLocalRandom.current().nextInt(0, width);
                    appleY = ThreadLocalRandom.current().nextInt(0, height);
                } while (isFieldOccupied(appleX, appleY));
                setField(appleX, appleY, apple);
                isApplePresent = true;
            }
        }

        return true;
    }

    private boolean isFieldOccupied(int x, int y) {
        return (x == headX && y == headY) || tail.stream().anyMatch(part -> part.a.equals(x) && part.b.equals(y));
    }

    private void setField(int x, int y, String symbol) {
        gameArea[x][y] = symbol;
    }
    private void setHead(int x, int y) {
        headX = x;
        headY = y;
        gameArea[headX][headY] = head;
    }

    public User getCreator() {
        return creator;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("**SNAKE** by BadToxic\n");
        if (!isEmojiMode) {
            sb.append(toStringText());
        } else {
            for (int yIndex = 0; yIndex < gameArea[0].length; yIndex++) {
                for (int xIndex = 0; xIndex < gameArea.length; xIndex++) {
                    sb.append(gameArea[xIndex][yIndex]);
                }
                sb.append("\n");
            }
        }
        sb.append("  Score: " + score + "\n");
        if (gameOver) {
            sb.append("Game Over!");
        } else if (!directions.isEmpty()) {
            directions.forEach(dir -> sb.append(directionToEmoji.get(dir)));
            sb.append("\n");
        }
        return sb.toString();
    }
    public String toStringText() {
        String result = "";
        for (int yIndex = -1; yIndex <= gameArea[0].length; yIndex++) {
            for (int xIndex = -1; xIndex <= gameArea.length; xIndex++) {
                if (xIndex < 0 || xIndex >= gameArea.length) {
                    if (xIndex < 0 && yIndex < 0 || xIndex >= gameArea.length && yIndex >= gameArea[0].length) {
                        result += '=';
                        continue;
                    }
                    result += '|';
                    continue;
                }
                if (yIndex < 0 || yIndex >= gameArea[0].length) {
                    result += '=';
                    continue;
                }
                result += gameArea[xIndex][yIndex];
            }
            result += "\n";
        }
        return result;
    }
}
