package de.badtoxic.gm_kub.discord.game.president;

import de.badtoxic.gm_kub.type.DiscordEmoji;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Object representing a player of the President (card game).
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2021-06-06
 */
public class DiscordGamePresidentPlayer {

    private User user;
    private Message directMessage;
    private String name;

    private List<DiscordEmoji> cards;
    private DiscordEmoji selectedCard;
    private int selectedAmount;
    private boolean skippedRound = false;

    public DiscordGamePresidentPlayer(User user, String name) {
        this.user = user;
        this.name = name;
        cards = new ArrayList<>();
        selectedCard = null;
        selectedAmount = 0;
    }

    public User getUser() {
        return user;
    }

    public String getId() {
        if (user == null) {
            return null;
        }
        return user.getId();
    }

    public String getName() {
        return name;
    }

    public void sortCards() {
        Collections.sort(cards, new PresidentCardComparator());
    }

    public List<DiscordEmoji> getCards() {
        return cards;
    }

    public DiscordEmoji getSelectedCard() {
        return selectedCard;
    }

    public void setSelectedCard(DiscordEmoji selectedCard) {
        this.selectedCard = selectedCard;
    }

    public int getSelectedAmount() {
        return selectedAmount;
    }

    public void setSelectedAmount(int selectedAmount) {
        this.selectedAmount = selectedAmount;
    }

    public Message getDirectMessage() {
        return directMessage;
    }

    public void setDirectMessage(Message directMessage) {
        this.directMessage = directMessage;
    }

    public boolean isSkippedRound() {
        return skippedRound;
    }

    public void setSkippedRound(boolean skippedRound) {
        this.skippedRound = skippedRound;
    }

    public boolean hasSelectedCards() {
        if (selectedCard == null || selectedAmount == 0) {
            return false;
        }
        int neededAmount = getSelectedAmount();
        for (DiscordEmoji card : cards) {
            if (selectedCard.equals(card)) {
                neededAmount--;
            }
        }
        return neededAmount <= 0;
    }

    // Clear current selection and remove these cards from the player's hand
    public void moveSelectedCardsToTable() {
        if (selectedCard == null || selectedAmount == 0) {
            return;
        }
        int neededAmount = getSelectedAmount();
        for (int cardIndex = cards.size() - 1; cardIndex >= 0; cardIndex--) {
            if (selectedCard.equals(cards.get(cardIndex))) {
                cards.remove(cardIndex);
                neededAmount--;
                if (neededAmount <= 0) {
                    break;
                }
            }
        }
        selectedCard = null;
        selectedAmount = 0;
        if (directMessage == null) {
            return;
        }
        directMessage.editMessage(toString()).queue();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("These are your cards:\n");
        boolean firstCard = true;
        for (DiscordEmoji card : cards) {
            if (firstCard) {
                sb.append(card.toCode());
                firstCard = false;
            } else {
                sb.append(", " + card.toCode());
            }
        }
        return sb.toString();
    }

    public class PresidentCardComparator implements Comparator<DiscordEmoji> {
        @Override
        public int compare(DiscordEmoji card1, DiscordEmoji card2) {
            return Integer.compare(card1.toPresidentValue(), card2.toPresidentValue());
        }
    }
}
