package de.badtoxic.gm_kub.discord.game;

import de.badtoxic.gm_kub.discord.events.*;
import de.badtoxic.gm_kub.discord.game.president.DiscordGamePresident;
import de.badtoxic.gm_kub.discord.game.snake.DiscordGameSnake;
import de.badtoxic.gm_kub.entity.DiscordHighscore;
import de.badtoxic.gm_kub.repository.DiscordHighscoreRepository;
import de.badtoxic.gm_kub.repository.ItemRepository;
import de.badtoxic.gm_kub.type.DirectionEnum;
import de.badtoxic.gm_kub.type.DiscordEmoji;
import de.badtoxic.gm_kub.type.DiscordGameEnum;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent;
import org.antlr.v4.runtime.misc.Pair;

import javax.annotation.Nullable;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.ObservesAsync;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.badtoxic.gm_kub.type.DiscordEmoji.*;

/**
 * Class for handling events like reactions in a Discord Game
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@ApplicationScoped
public class DiscordGameHandler {

    private Map<DiscordEmoji, DirectionEnum> emojiToDirection = new HashMap<>(){{
        put(ArrowUp, DirectionEnum.Up);
        put(ArrowRight, DirectionEnum.Right);
        put(ArrowDown, DirectionEnum.Down);
        put(ArrowLeft, DirectionEnum.Left);
    }};
    private Map<String, DiscordGame> messageToGame = new HashMap<>();

    @Inject
    private DiscordHighscoreRepository discordHighscoreRepository;

    @Inject
    @EndDiscordGame
    private Event<DiscordHighscore> endGameEvent;

    public void startGame(MessageReceivedEvent event, String gameName) {
        DiscordGameEnum discordGameEnum = DiscordGameEnum.fromString(gameName);
        if (discordGameEnum != null) {
            String msg = event.getMessage().getContentRaw();
            int argsBeginIndex = msg.indexOf(gameName) + gameName.length() + 1;
            String gameArgs = msg.length() > argsBeginIndex ? msg.substring(argsBeginIndex) : "";

            if (gameArgs.contains("highscore")) {
                String finalGameName = gameName;
                event.getChannel().sendMessage("Checking Highscores for " + gameName + "...").queue(response -> {
                    List<DiscordHighscore> scoreList = discordHighscoreRepository.findByDiscordGameName(discordGameEnum);
                    if (scoreList == null || scoreList.isEmpty()) {
                        response.editMessage("No Highscores found for " + finalGameName).queue();
                        return;
                    }
                    StringBuilder sb = new StringBuilder();
                    scoreList.forEach(hs -> {
                        sb.append(hs.getScore());
                        sb.append("    ");
                        try {
                            Member member = event.getGuild().retrieveMemberById(hs.getDiscordUserID()).complete();
                            sb.append(member.getNickname());
                        } catch (Exception exc) {
                            sb.append(hs.getDiscordUserName());
                        }
                        sb.append("\n");
                    });
                    response.editMessage("Highscores for " + finalGameName + ":\n" + sb.toString()).queue();
                });
                return;
            }

            DiscordGame discordGame = null;
            switch (discordGameEnum) {
                case SNAKE: {
                    discordGame = new DiscordGameSnake(this, event.getAuthor(), gameArgs);
                    break;
                }
                case PRESIDENT: {
                    discordGame = new DiscordGamePresident(this, event.getAuthor(), gameArgs);
                    break;
                }
            }
            if (discordGame != null) {
                DiscordGame finalDiscordGame = discordGame;
                event.getChannel().sendMessage(discordGame.toString()).queue(
                        response -> messageToGame.put(finalDiscordGame.init(response), finalDiscordGame)
                );
                return;
            }
        }
        if(gameName.length() > 20) {
            gameName = gameName.substring(0, 18) + "...";
        }
        event.getChannel().sendMessage("I don't know the game " + gameName).queue();
    }

    public void observeMessageReactionAdd(@ObservesAsync @MessageReactionAdd MessageReactionAddEvent event){
        onReaction(event.getMessageId(), event.getReactionEmote(), event.getUserId(), getReactionUserName(event), event.getUser());
    }
    public void observeMessageReactionRemove(@ObservesAsync @MessageReactionRemove MessageReactionRemoveEvent event){
        onReaction(event.getMessageId(), event.getReactionEmote(), event.getUserId(), getReactionUserName(event), event.getUser());
    }
    public void observeGuildMessageDelete(@ObservesAsync @GuildMessageDelete GuildMessageDeleteEvent event){
        String messageID = event.getMessageId();
        if (!messageToGame.containsKey(messageID)) {
            return;
        }
        DiscordGame game = messageToGame.get(messageID);
        game.stop(true);
    }
    private String getReactionUserName(GenericMessageReactionEvent event) {
        String name = null;
        if (event.getMember() != null) {
            name = event.getMember().getEffectiveName();
        }
        if (name == null) {
            if (event.getUser() != null) {
                name = event.getUser().getName();
            }
            if (name == null) {
                name = "Unknown Player";
            }
        }
        return name;
    }
    public void observeStartGame(@ObservesAsync @StartDiscordGame Pair<MessageReceivedEvent, String> eventGameNamePair){
        startGame(eventGameNamePair.a, eventGameNamePair.b);
    }

    private void onReaction(String messageID, MessageReaction.ReactionEmote emote,
                            String userId, String userName, @Nullable User user) {
        DiscordEmoji emoji = fromString(emote.toString().replace("RE:", ""));
        if (!messageToGame.containsKey(messageID)) {
            return;
        }
        DiscordGame game = messageToGame.get(messageID);
        if (game.isOnlyCreatorReactions() && !game.getCreator().getId().equals(userId)) {
            return;
        }
        if (emojiToDirection.containsKey(emoji)) {
            DirectionEnum direction = emojiToDirection.get(emoji);
            game.setDirection(direction);
            return;
        }
        if (PausePlay.equals(emoji)) {
            game.pause();
            return;
        }
        if (RedX.equals(emoji) && game.getCreator().getId().equals(userId)) {
            System.out.println("Stop game");
            game.stop(false);
            return;
        }
        if (ArrowCircle.equals(emoji) && game.getCreator().getId().equals(userId)) {
            game.restart();
            return;
        }
        game.handleReaction(emoji, userId, userName, user);
    }

    public void onGameStopped(Message message) {
        DiscordGame stoppedGame = messageToGame.remove(message.getId());
        long score = stoppedGame.getScore();
        if (score > 0) {
            DiscordGameEnum gameName = stoppedGame.getDiscordGameName();
            User creator = stoppedGame.getCreator();

            // First load old highscores
            List<DiscordHighscore> scoreList = discordHighscoreRepository.findByDiscordGameName(gameName);

            // Save new score if it's better than my previous
            String creatorName = creator.getName();
            creatorName = creatorName.substring(0, Math.min(creatorName.length(), 32));
            endGameEvent.fireAsync(new DiscordHighscore(gameName, creator.getId(), creatorName, score));

            // Check highscore position results
            String scoreResult = "";
            if (scoreList == null || scoreList.isEmpty()) {
                scoreResult = "You are the first in this highscore list!";
            } else {
                int pos = 1, posPrev = 0, posCount = 1;
                for (DiscordHighscore dh : scoreList) {
                    // Compare with myself
                    if (dh.getDiscordUserID().equals(creator.getId())) {
                        posPrev = posCount; // Found old position
                        if (dh.getScore() >= score) {
                            scoreResult = "You could not beat your best score of " + dh.getScore() +
                                        ". You should try again! ";
                            pos = posPrev;
                            break;
                        } else {
                            scoreResult = "You've beaten your own record score of " + dh.getScore() + ".\n";
                        }
                    }

                    // Increase counter
                    if (dh.getScore() > score) {
                        pos++;
                    }
                    posCount++;
                }
                boolean kicked = false;
                if (posPrev > 0) {
                    if (pos < posPrev) {
                        scoreResult += "You climbed from position " + posPrev + " to " + pos + "! Congratulations!\n";
                        kicked = true;
                    }
                } else {
                    scoreResult += "You placed on position " + pos + "! Nice!\n";
                    kicked = true;
                }
                if (kicked) {
                    DiscordHighscore kickedHS = scoreList.get(pos - 1);
                    String kickedName;
                    if (kickedHS.getScore() == score) {
                        kickedName = "";
                        for (DiscordHighscore dh : scoreList) {
                            if (dh.getScore() == score) {
                                String nextKickedName;
                                try {
                                    Member member = message.getGuild().retrieveMemberById(kickedHS.getDiscordUserID()).complete();
                                    nextKickedName = " @" + member.getNickname();
                                } catch (Exception exc) {
                                    nextKickedName = " " + kickedHS.getDiscordUserName();
                                }
                                if (!kickedName.contains(nextKickedName)) {
                                    kickedName += nextKickedName;
                                }
                            }
                        }
                        scoreResult += "You share this position with" + kickedName + "! Maybe someone here should make a better score, too?\n";
                    } else {
                        try {
                            Member member = message.getGuild().retrieveMemberById(kickedHS.getDiscordUserID()).complete();
                            kickedName = member.getNickname();
                        } catch (Exception exc) {
                            kickedName = kickedHS.getDiscordUserName();
                        }
                        scoreResult += "You kicked @" + kickedName + " from his position! I bet this user wants revenge!\n";
                    }
                }
            }
            scoreResult = message.getContentRaw() + "\n" + scoreResult;
            message.editMessage(scoreResult.substring(0, Math.min(scoreResult.length(), 1999))).queue();
        }
    }
}
