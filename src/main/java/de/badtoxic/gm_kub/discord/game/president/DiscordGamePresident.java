package de.badtoxic.gm_kub.discord.game.president;

import de.badtoxic.gm_kub.discord.game.DiscordGame;
import de.badtoxic.gm_kub.discord.game.DiscordGameHandler;
import de.badtoxic.gm_kub.type.DirectionEnum;
import de.badtoxic.gm_kub.type.DiscordEmoji;
import de.badtoxic.gm_kub.type.DiscordGameEnum;
import net.dv8tion.jda.api.entities.User;

import javax.annotation.Nullable;
import java.util.*;

import static de.badtoxic.gm_kub.type.DiscordEmoji.*;

/**
 * President (card game)
 * President (also commonly called Asshole, Scum, or Capitalism) is a westernized version of an originally Japanese
 * card game named daifugō or daihinmin. It is a game for three or more, in which the players race to get rid of all of
 * the cards in their hands in order to become "president" in the following round.
 *
 * President can also be played as a drinking game, and a commercial version of the game exists under the name
 * "The Great Dalmuti", with a non-standard deck.
 * [Wikipedia]
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2021-06-06
 */
public class DiscordGamePresident extends DiscordGame {

    private final int playersMin = 2;
    private final int playersMax = 8;

    private Map<String, DiscordGamePresidentPlayer> players = new HashMap<>();
    private List<DiscordGamePresidentPlayer> playersList = new ArrayList<>();

    private boolean gamePrepared; // If preparations are done
    private int playerTurn = 0;
    private String table;
    private int topValue = 0; // Value of the cards played last
    private String lastAction; // Holds text about what happened last.
    private DiscordGamePresidentPlayer winner;

    public DiscordGamePresident(DiscordGameHandler gameHandler, User creator, String arguments) {
        super(gameHandler, creator);
        discordGameName = DiscordGameEnum.PRESIDENT;
        onlyCreatorReactions = false;
        neededReactionsBeforePreparation = new ArrayList<>();
        neededReactionsBeforePreparation.add(0, CheckMarkGreen);
        neededReactionsAfterPreparation = new ArrayList<>();
        neededReactionsAfterPreparation.add(Num3);
        neededReactionsAfterPreparation.add(Num4);
        neededReactionsAfterPreparation.add(Num5);
        neededReactionsAfterPreparation.add(Num6);
        neededReactionsAfterPreparation.add(Num7);
        neededReactionsAfterPreparation.add(Num8);
        neededReactionsAfterPreparation.add(Num9);
        neededReactionsAfterPreparation.add(Num10);
        neededReactionsAfterPreparation.add(LetterJ);
        neededReactionsAfterPreparation.add(LetterQ);
        neededReactionsAfterPreparation.add(LetterK);
        neededReactionsAfterPreparation.add(LetterA);
        neededReactionsAfterPreparation.add(Num2);
        neededReactionsAfterPreparation.add(Joker);
        neededReactionsAfterPreparation.add(TrackNext); // Pass
        handleArguments(arguments);
        start();
    }

    private void handleArguments(String arguments) {
        /*String[] msgs = arguments.split(" ");
        errors = "";
        for (int msgIndex = 0; msgIndex < msgs.length; msgIndex++) {
            String msg = msgs[msgIndex];
            switch (msg) {
                case "text": {
                    isEmojiMode = false;
                    break;
                }
            }
        }*/
    }

    @Override
    protected void start() {
        startBase();
        stepDuration = 0; // No automatic running
        gamePrepared = false;
        table = "";
        lastAction = "";
        topValue = 0;
        players.clear();
        playersList.clear();
        winner = null;
        DiscordGamePresidentPlayer creatorPlayer = new DiscordGamePresidentPlayer(creator, creator.getName());
        players.put(creator.getId(), creatorPlayer);
        playersList.add(creatorPlayer);
    }

    @Override
    public void handleReaction(DiscordEmoji emoji, String userId, String userName, @Nullable User user) {
        System.out.println("President - handleReaction: " + emoji); // TODO: Remove this line
        switch (emoji) {
            // Player joins/leaves the game
            case CheckMarkGreen: {
                // Do nothing if the game is already prepared or the creator wants to join/leave (he has to play)
                if (gamePrepared || userId.equals(creator.getId())) {
                    if (!gamePrepared) {
                        lastAction = creator.getName() + ", you can not leave your own game.";
                        run(message); // Trigger refresh
                    }
                    return;
                }
                if (players.containsKey(userId)) {
                    playersList.remove(players.get(userId));
                    players.remove(userId);
                    if (players.entrySet().size() == 1) {
                        message.removeReaction(OK.toString()).queue();
                    }
                } else {
                    if (user == null) {
                        // We need that object here. We may not get it on reaction removal.
                        return;
                    }
                    if (playersList.size() >= playersMax) {
                        lastAction = "Sorry " + creator.getName() + ", the game is already full.";
                        run(message); // Trigger refresh
                        return;
                    }
                    DiscordGamePresidentPlayer newPlayer = new DiscordGamePresidentPlayer(user, userName);
                    players.put(userId, newPlayer);
                    playersList.add(newPlayer);
                    if (players.entrySet().size() == 2) {
                        message.addReaction(OK.toString()).queue();
                    }
                }
                run(message); // Trigger refresh
                break;
            }
            case OK: {
                if (gamePrepared || !userId.equals(creator.getId())) {
                    if (!gamePrepared) {
                        lastAction = "Only the host " + creator.getName() + " can start the game.";
                        run(message); // Trigger refresh
                    } else {
                        DiscordGamePresidentPlayer player = playersList.get(playerTurn);
                        if (!userId.equals(player.getId()) || player.isSkippedRound()) {
                            return;
                        }
                        // Player validates his selection
                        checkPlayerSelection(player);
                        run(message); // Trigger refresh
                    }
                    return;
                }
                // Start the game
                finishedPreparations();
                gamePrepared = true;
                Random random = new Random();
                playerTurn = random.nextInt(playersList.size());
                run(message); // Trigger refresh
                generateCards();
                break;
            }
            case Num2:
            case Num3:
            case Num4:
            case Num5:
            case Num6:
            case Num7:
            case Num8:
            case Num9:
            case Num10:
            case LetterJ:
            case LetterQ:
            case LetterK:
            case LetterA:
            case Joker: {
                if (!gamePrepared) {
                    return;
                }
                DiscordGamePresidentPlayer player = playersList.get(playerTurn);
                if (!userId.equals(player.getId()) || player.isSkippedRound()) {
                    return;
                }
                setOrRemoveCard(player, emoji);

                run(message); // Trigger refresh
                break;
            }
            case TrackNext: { // Skip round
                if (!gamePrepared) {
                    return;
                }
                DiscordGamePresidentPlayer player = playersList.get(playerTurn);
                if (!userId.equals(player.getId())) {
                    return;
                }

                pass(player);

                run(message); // Trigger refresh
                break;
            }
        }
    }
    private void setOrRemoveCard(DiscordGamePresidentPlayer player, DiscordEmoji emoji) {
        // Same card type
        if (emoji.equals(player.getSelectedCard())) {
            if (DiscordEmoji.Joker.equals(emoji)) {
                // Nothing to do if it is a Joker
                return;
            }
            int amount = player.getSelectedAmount() + 1;
            // A maximum of 4 cards can be selected. Else start from 1 again.
            if (amount > 4) {
                amount = 1;
            }
            player.setSelectedAmount(amount);
            return;
        }
        // Change to another card
        player.setSelectedCard(emoji);
        player.setSelectedAmount(1);
    }
    private void checkPlayerSelection(DiscordGamePresidentPlayer player) {
        if (player.getSelectedCard() == null || player.getSelectedAmount() <= 0) {
            return;
        }
        if (!player.hasSelectedCards()) {
            lastAction = player.getName() + ", you don't have the selected cards.";
            return;
        }

        DiscordEmoji cardEmoji = player.getSelectedCard();

        // Add card values = [1 - 13 | 100]      +             [0 | 25 | 50]
        int value = cardEmoji.toPresidentValue() + ((player.getSelectedAmount() - 1) * 13);
        if (value <= topValue) {
            lastAction = player.getName() + ", the value of this is lower than the previous.";
            return;
        }

        // Put cards on Table
        if (!"".equals(table)) {
            table += ", ";
        }
        int amount = player.getSelectedAmount();
        for (int amountIndex = 0; amountIndex < amount; amountIndex++) {
            table += cardEmoji.toCode();
        }
        topValue = value;
        lastAction = player.getName() + " has placed " +
                     (amount > 1 ? amount : "one") + " card" + (amount > 1 ? "s." : ".");

        // Clear current selection and remove these cards from the player's hand. Send direct message update.
        player.moveSelectedCardsToTable();

        // Check if player has won
        if (player.getCards().isEmpty()) {
            winner = player;
            gameOver = true;

            message.removeReaction(OK.toString()).queue();

            // Remove player direct messages
            for (DiscordGamePresidentPlayer otherPlayer : playersList) {
                otherPlayer.getDirectMessage().delete().queue();
            }

            return;
        }

        // If player placed a Joker, the round is ended and he keeps the turn
        if (Joker.equals(cardEmoji)) {
            startNewRound();
            return;
        }

        // Next turn (skipping players that skipped the round)
        while (true) {
            playerTurn++;
            if (playerTurn >= playersList.size()) {
                playerTurn = 0;
            }
            if (!playersList.get(playerTurn).isSkippedRound()) {
                break;
            }
        }
    }

    // Player passes a round
    private void pass(DiscordGamePresidentPlayer player) {
        player.setSkippedRound(true);
        player.setSelectedCard(null);
        player.setSelectedAmount(0);

        // Check if all players skipped, to start a new round
        boolean allSkipped = true;
        for (DiscordGamePresidentPlayer otherPlayer : playersList) {
            if (!otherPlayer.isSkippedRound()) {
                allSkipped = false;
                break;
            }
        }

        // Clean the table and start a new round (player last skipping can start the next round)
        if (allSkipped) {
            lastAction = "All players skipped. A new round was started.";
            startNewRound();
            return;
        }

        lastAction = player.getName() + " skipped.";

        // Next turn
        playerTurn++;
        if (playerTurn >= playersList.size()) {
            playerTurn = 0;
        }
    }

    private void startNewRound() {
        table = "";
        topValue = 0;
        for (DiscordGamePresidentPlayer otherPlayer : playersList) {
            otherPlayer.setSkippedRound(false);
        }
    }

    private void generateCards() {
        List<DiscordEmoji> cards = new ArrayList<>();
        // 1=(3), 2=(4), 3=(5), 4=(6), 5=(7), 6=(8), 7=(9), 8=(10), 9=J(Jack/Bube), 10=Q(Queen), 11=K(King), 12=A(Ace), 13=(2), 15=W(Joker)
        for (int value = 1; value <= 13; value++) {
            for (int number = 0; number < 4; number++) {
                cards.add(DiscordEmoji.fromPresidentValue(value));
            }
        }
        cards.add(Joker);
        cards.add(Joker);
        Collections.shuffle(cards);

        while (!cards.isEmpty()) {
            // Distribute cards
            for (DiscordGamePresidentPlayer player : playersList) {
                player.getCards().add(cards.get(0));
                cards.remove(0);
            }
            // Stop if not enough cards are left to give one to each player
            if (cards.size() < playersList.size()) {
                break;
            }
        }

        // Notify every player about his cards and sort them
        for (DiscordGamePresidentPlayer player : playersList) {
            player.sortCards();
            player.getUser().openPrivateChannel()
                  .flatMap(channel -> channel.sendMessage(player.toString()))
                  .queue(message -> player.setDirectMessage(message));
        }
    }

    @Override
    protected boolean nextStep() {
        return true;
    }

    @Override
    public void setDirection(DirectionEnum direction) {
        // Nothing to do
    }

    public User getCreator() {
        return creator;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("**PRESIDENT** by BadToxic\n");
        if (!gamePrepared && !gameOver) {
            sb.append(":white_check_mark:: Join the game (" + playersMin + "-" + playersMax + " Players)\n");
            if (playersList.size() < 2) {
                sb.append("At least one more player is needed.\n");
            } else {
                sb.append(":ok:: Start the game (Host " + creator.getName() + ")\n");
            }
        }
        if (!players.isEmpty()) {
            sb.append("Players: ");
            int playerIndex = 0;
            for (DiscordGamePresidentPlayer player : players.values()) {
                if (gamePrepared && !gameOver && playerIndex == playerTurn) {
                    sb.append(":arrow_right:**" + player.getName() + "**");
                } else if (gamePrepared && gameOver && player.equals(winner)) {
                    sb.append(":crown:**" + player.getName() + "**");
                } else {
                    sb.append(player.getName());
                }
                if (playerIndex < playersList.size() - 1) {
                    sb.append(", ");
                }
                playerIndex++;
            }
            sb.append("\n");

            if (gamePrepared) {
                // Cards on table
                if (!"".equals(table)) {
                    sb.append("Table: " + table + "\n");
                }
            }

            // What happened last
            if (!"".equals(lastAction)) {
                sb.append(lastAction + "\n");
            }

            // Currently selected cards
            if (gamePrepared && !gameOver && playersList.size() > playerTurn) {
                DiscordGamePresidentPlayer playerCurrent = playersList.get(playerTurn);
                if (playerCurrent.getSelectedCard() != null && playerCurrent.getSelectedAmount() != 0) {
                    String selection = playerCurrent.getName() + "'s selection: ";
                    for (int amountIndex = 0; amountIndex < playerCurrent.getSelectedAmount(); amountIndex++) {
                        selection += playerCurrent.getSelectedCard().toCode();
                    }
                    sb.append(selection + "\n");
                    sb.append(":ok:: Confirm selection\n");
                } else {
                    sb.append(playerCurrent.getName() + " must select one to four cards of the same type.\n");
                }
            }
        }
        if (gameOver) {
            sb.append("Game Over!");
            if (winner != null) {
                sb.append(" **" + winner.getName() + " has won the game!**");
            }
        }
        return sb.toString();
    }
}
