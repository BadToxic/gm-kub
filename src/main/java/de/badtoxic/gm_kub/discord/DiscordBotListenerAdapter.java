package de.badtoxic.gm_kub.discord;

import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

/**
 * Class extending JDA listeners for a Discord bot
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class DiscordBotListenerAdapter extends ListenerAdapter {

    private DiscordBot discordBot;

    public DiscordBotListenerAdapter(DiscordBot discordBot) {
        this.discordBot = discordBot;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.isFromType(ChannelType.PRIVATE)) {
            System.out.printf("[PM] %s: %s\n", event.getAuthor().getName(),
                    event.getMessage().getContentDisplay());
        } else {
            MessageChannel channel = event.getChannel();
            System.out.printf("[%s][%s] %s (%s): %s\n", event.getGuild().getName(),
                    channel.getName(), event.getMember().getEffectiveName(), event.getMember().getId(),
                    event.getMessage().getContentDisplay());
        }
        discordBot.onMessageReceived(event);
    }
    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        discordBot.onGuildMessageReceived(event);
    }
    @Override
    public void onGuildMessageDelete(GuildMessageDeleteEvent event) {
        discordBot.onGuildMessageDelete(event);
    }
    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        MessageReaction.ReactionEmote emote = event.getReactionEmote();
        System.out.println("onMessageReactionAdd" + emote + ", Message ID: " + event.getMessageId());
        if (event.getReaction().isSelf()) {
            return; // Ignore own reactions
        }
        discordBot.onMessageReactionAdd(event);
    }
    @Override
    public void onMessageReactionRemove(MessageReactionRemoveEvent event) {
        MessageReaction.ReactionEmote emote = event.getReactionEmote();
        System.out.println("onMessageReactionRemove" + emote + ", Message ID: " + event.getMessageId());
        if (event.getReaction().isSelf()) {
            return; // Ignore own reactions
        }
        discordBot.onMessageReactionRemove(event);
    }
}
