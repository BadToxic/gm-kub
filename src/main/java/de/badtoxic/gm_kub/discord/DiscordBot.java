package de.badtoxic.gm_kub.discord;

import de.badtoxic.gm_kub.discord.events.GuildMessageDelete;
import de.badtoxic.gm_kub.discord.events.MessageReactionAdd;
import de.badtoxic.gm_kub.discord.events.MessageReactionRemove;
import de.badtoxic.gm_kub.discord.events.StartDiscordGame;
import de.badtoxic.gm_kub.entity.BaseEntity;
import de.badtoxic.gm_kub.repository.DeviceRepository;
import de.badtoxic.gm_kub.repository.ItemRepository;
import de.badtoxic.gm_kub.repository.MiniGameRepository;
import de.badtoxic.gm_kub.type.CommandEnum;
import de.badtoxic.gm_kub.type.DiscordGameEnum;
import de.badtoxic.gm_kub.type.EntityTypeEnum;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.antlr.v4.runtime.misc.Pair;
import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;

import javax.annotation.Nonnull;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.security.auth.login.LoginException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static de.badtoxic.gm_kub.discord.DiscordMessageBuilder.*;

/**
 * Heart of the Discord bot
 *
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
@ApplicationScoped
public class DiscordBot {

    private static String activity = "GameMaster";

    private MiniGameRepository miniGameRepository;
    private DeviceRepository deviceRepository;
    private ItemRepository itemRepository;
    private DiscordMessageSender messageSender;

    @Inject
    @MessageReactionAdd
    private Event<MessageReactionAddEvent> messageReactionAddEvent;
    @Inject
    @MessageReactionRemove
    private Event<MessageReactionRemoveEvent> messageReactionRemoveEvent;
    @Inject
    @StartDiscordGame
    private Event<Pair<MessageReceivedEvent, String>> startGameEvent;
    @Inject
    @GuildMessageDelete
    private Event<GuildMessageDeleteEvent> guildMessageDeleteEvent;

    @Inject
    @ConfigurationValue("discord.token")
    String discordToken;
    @Inject
    @ConfigurationValue("discord.command")
    String botCommand;

    public DiscordBot() {}

    public void init(@Observes @Initialized(ApplicationScoped.class) Object init,
                     MiniGameRepository miniGameRepository, DeviceRepository deviceRepository,
                     ItemRepository itemRepository,
                     DiscordMessageSender messageSender) throws LoginException {
        this.miniGameRepository = miniGameRepository;
        this.deviceRepository = deviceRepository;
        this.itemRepository = itemRepository;
        this.messageSender = messageSender;
        System.out.println("Init Discord Bot (Command: " + botCommand + ")");
        JDABuilder.createLight(discordToken,
                GatewayIntent.DIRECT_MESSAGES, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MEMBERS,
                GatewayIntent.DIRECT_MESSAGE_REACTIONS, GatewayIntent.GUILD_MESSAGE_REACTIONS)
            .addEventListeners(new DiscordBotListenerAdapter(this))
            .setActivity(Activity.playing(activity))
            .build();
    }

    public void onMessageReceived(MessageReceivedEvent event) {
        String[] msg = event.getMessage().getContentRaw().split(" ");
        MessageChannel channel = event.getChannel();

        if (msg[0].equalsIgnoreCase("!ping")) {
            messageSender.sendPong(channel);
        }
        if (msg[0].equalsIgnoreCase("!tree")) {
            try {
                Files.find(Paths.get("."),
                        Integer.MAX_VALUE,
                        (filePath, fileAttr) -> fileAttr.isRegularFile() && filePath.endsWith("project-defaults.yml"))
                        .forEach(System.out::println);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        if (msg[0].equalsIgnoreCase(botCommand)) {
            handleBotCommands(event);
        }
    }
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] msg = event.getMessage().getContentRaw().split(" ");
        if (msg[0].equalsIgnoreCase(botCommand)) {
            handleGuildBotCommands(event);
        }
    }
    public void onGuildMessageDelete(GuildMessageDeleteEvent event) {
        guildMessageDeleteEvent.fireAsync(event);
    }
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        messageReactionAddEvent.fireAsync(event);
    }
    public void onMessageReactionRemove(MessageReactionRemoveEvent event) {
        messageReactionRemoveEvent.fireAsync(event);
    }

    /**
     * Handle bot commands that work everywhere - in guilds (=servers) and via private messages
     * @param event
     */
    private void handleBotCommands(MessageReceivedEvent event) {
        String msgRaw = event.getMessage().getContentRaw();
        if (msgRaw.length() < botCommand.length() + 2) {
            messageSender.sendMissingCommand(event);
            return;
        }
        String[] msg = msgRaw.split(" ", 2)[1].split(" ");
        MessageChannel channel = event.getChannel();
        String argument1 = msg[0].toLowerCase();
        CommandEnum command = CommandEnum.fromString(argument1);
        if (command != null) {
            switch (command) {
                case HELP: {
                    messageSender.sendHelp(event);
                    return;
                }
                case INVITE: {
                    if (event.isFromType(ChannelType.PRIVATE)) {
                        channel.sendMessage("You must write from within a server to request an invitation.").queue();
                    }
                    return;
                }
                case REPEAT: {
                    messageSender.sendRepeat(event, msg[1]);
                    return;
                }
                case START: {
                    startGameEvent.fireAsync(new Pair<>(event, msg[1]));
                    return;
                }
            }
        }
        DiscordGameEnum discordGameEnum = DiscordGameEnum.fromString(argument1);
        if (discordGameEnum != null) {
            startGameEvent.fireAsync(new Pair<>(event, argument1));
            return;
        }
        EntityTypeEnum entityType = EntityTypeEnum.fromString(argument1);
        if (entityType != null) {
            handleEntity(entityType, event);
            return;
        }
        if ("s".equalsIgnoreCase(argument1.substring(argument1.length() - 1))){
            entityType = EntityTypeEnum.fromString(argument1.substring(0, argument1.length() - 1));
            if (entityType != null) {
                handleEntities(entityType, event);
                return;
            }
        }
        messageSender.sendUnknownCommand(event, argument1);
    }

    /**
     * Handle bot commands that only work in guilds (=servers)
     * @param event
     */
    private void handleGuildBotCommands(GuildMessageReceivedEvent event) {
        String[] msg = getMessageReduced(event, 2);
        switch (msg[0]) {
            case "invite": {
                messageSender.sendInvite(event);
                break;
            }
        }
    }

    private void handleEntity(EntityTypeEnum entityType, MessageReceivedEvent event) {
        String[] msg = getMessageReduced(event, 3);
        if (msg.length < 1) {
            event.getChannel().sendMessage("To load information about a " + entityType + " you must provide the **ID** or the **name**.").queue();
            return;
        }
        String argument1 = msg[0];
        Object key;
        String keyType;
        if (argument1.matches("(0|[1-9]\\d*)")) {
            try {
                key = Integer.parseInt(argument1);
            } catch (NumberFormatException exception) {
                event.getChannel().sendMessage("The number " + argument1 + " can't be interpreted as ID.").queue();
                return;
            }
            keyType = "ID";
        } else {
            String keyStr = String.join(" ", msg);
            if (keyStr.length() > 32) {
                event.getChannel().sendMessage("This name with " + keyStr.length() + " chars is too long. " +
                        "Names can have 32 chars at most.").queue();
                return;
            }
            key = keyStr;
            keyType = "name";
        }
        String finalKeyType = keyType;
        event.getChannel().sendMessage("Loading information about " + entityType + " with " + keyType + " " + key).queue(
            response -> {
                BaseEntity entity;
                try {
                    entity = find(entityType, key);
                } catch (Exception exception) {
                    response.editMessage("An exception occurred while connecting to the database to find " +
                            entityType + " with " + finalKeyType + " " + key + ": " + exception.getMessage()).queue();
                    return;
                }
                if (entity == null) {
                    response.editMessage("Could not find " + entityType + " with " + finalKeyType + " **" + key + "**").queue();
                    return;
                }
                response.editMessage("Information about " + entityType + " with " + finalKeyType + " **" + key + "**").queue();
                response.editMessage(buildEntityMessage(entityType, entity)).queue();
            }
        );
    }
    private void handleEntities(EntityTypeEnum entityType, MessageReceivedEvent event) {
        event.getChannel().sendMessage("Loading list of " + entityType + "s.").queue(
            response -> {
                List<BaseEntity> entities = findAll(entityType);
                if (entities == null || entities.isEmpty()) {
                    response.editMessage("Could not find " + entityType + "s!").queue();
                    return;
                }
                response.editMessage("The following " + entityType + "s exist:").queue();
                response.editMessage(buildEntitiesMessage(entities)).queue();
            }
        );
    }
    private BaseEntity find(EntityTypeEnum entityType, Object entityKey) {
        if (entityKey instanceof Integer) {
            return findID(entityType, (Integer)entityKey);
        }
        String entityKeyStr = (String) entityKey;
        if (entityKeyStr.length() > 32) {
            return null;
        }
        return findName(entityType, entityKeyStr);
    }
    private BaseEntity findID(EntityTypeEnum entityType, int entityId) {
        switch (entityType) {
            case MiniGame: {
                return miniGameRepository.find(entityId);
            }
            case Device: {
                return deviceRepository.find(entityId);
            }
            case Item: {
                return itemRepository.find(entityId);
            }
        }
        return null;
    }
    private BaseEntity findName(EntityTypeEnum entityType, String entityName) {
        switch (entityType) {
            case MiniGame: {
                return miniGameRepository.findByName(entityName);
            }
            case Device: {
                return deviceRepository.findByName(entityName);
            }
            case Item: {
                return itemRepository.findByName(entityName);
            }
        }
        return null;
    }
    private List<BaseEntity> findAll(EntityTypeEnum entityType) {
        switch (entityType) {
            case MiniGame: {
                return miniGameRepository.findAll()
                        .stream().map(entity -> (BaseEntity)entity).collect(Collectors.toList());
            }
            case Device: {
                return deviceRepository.findAll()
                        .stream().map(entity -> (BaseEntity)entity).collect(Collectors.toList());
            }
            case Item: {
                return itemRepository.findAll()
                        .stream().map(entity -> (BaseEntity)entity).collect(Collectors.toList());
            }
        }
        return null;
    }
    private String[] getMessageReduced(GuildMessageReceivedEvent event, int reducedTo) {
        return event.getMessage().getContentRaw().split(" ", reducedTo)[reducedTo - 1].split(" ");
    }
    private String[] getMessageReduced(MessageReceivedEvent event, int reducedTo) {
        String[] msg = event.getMessage().getContentRaw().split(" ", reducedTo);
        if (msg.length < reducedTo) {
            return new String[0];
        }
        return msg[reducedTo - 1].split(" ");
    }
}
