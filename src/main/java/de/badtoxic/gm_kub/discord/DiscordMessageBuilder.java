package de.badtoxic.gm_kub.discord;

import de.badtoxic.gm_kub.entity.BaseEntity;
import de.badtoxic.gm_kub.entity.Device;
import de.badtoxic.gm_kub.entity.Item;
import de.badtoxic.gm_kub.entity.MiniGame;
import de.badtoxic.gm_kub.type.EntityTypeEnum;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Invite;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author  BadToxic (Michael Grönert)
 * @since   2020
 */
public class DiscordMessageBuilder {

    public static MessageEmbed buildHowToGetHelpMessage() {
        return new EmbedBuilder().setColor(Color.cyan)
                .setTitle("*If you need help, type:*")
                .setDescription("**gm! help**")
                .build();
    }
    public static MessageEmbed buildHelpMessage() {
        return new EmbedBuilder()
            .setColor(Color.cyan)
            .setTitle("**Commands** *starting with* **gm!**")
            .setDescription("**Create Invitation** : *invite*\n\n" /*+
                    "**Show Entity Info** : *(game|device|item) (name|id)*\n\n" +
                    "**List Entities** : *(games|devices|items)*\n\n"*/)
            .build();
    }
    public static MessageEmbed buildInvitationMessage(Guild guild, Invite invite) {
        String owner = (guild.getOwner() == null) ? "" : 
                "**The Owner is** : " + guild.getOwner().getAsMention() + "\n\n";
        return new EmbedBuilder()
            .setColor(Color.cyan)
            .setTitle("*Guild Info for* **" + guild.getName() + "**!")
            .setDescription("**Guild Invite** : " + invite.getUrl() + "\n\n" +
                    "**Total of Users** : " + guild.getMembers().size() + "\n\n" +
                    owner)
            .build();
    }
    private static EmbedBuilder getBaseEntityMessageBuilder(BaseEntity baseEntity) {
        return new EmbedBuilder().setImage(baseEntity.getImage()).setColor(Color.cyan);
    }
    public static MessageEmbed buildMiniGameMessage(MiniGame miniGame) {
        return getBaseEntityMessageBuilder(miniGame)
            .setTitle("*Mini Game Info for* **" + miniGame.getName() + "**!")
            .setDescription("**ID** : " + miniGame.getId() + "\n\n")
            .build();
    }
    public static MessageEmbed buildDeviceMessage(Device device) {
        return getBaseEntityMessageBuilder(device)
            .setTitle("*Device Info for* **" + device.getName() + "**!")
            .setDescription("**ID** : " + device.getId() + "\n\n")
            .build();
    }
    public static MessageEmbed buildItemMessage(Item item) {
        return getBaseEntityMessageBuilder(item)
            .setTitle("*Item Info for* **" + item.getName() + "**!")
            .setDescription("**ID** : " + item.getId() + "\n\n" +
                "**Description** : " + item.getDescription() + "\n\n" +
                "**Item Type** : " + item.getItemType() + "\n\n" +
                (item.getConsoleType() != null ? "**Console Type** : " + item.getConsoleType() + "\n\n" : "") +
                (item.getDataHolderType() != null ? "**Data Holder Type** : " + item.getDataHolderType() + "\n\n" : ""))
            .build();
    }
    public static MessageEmbed buildEntityMessage(EntityTypeEnum entityType, BaseEntity entity) {
        switch (entityType) {
            case MiniGame: {
                return buildMiniGameMessage((MiniGame)entity);
            }
            case Device: {
                return buildDeviceMessage((Device)entity);
            }
            case Item: {
                return buildItemMessage((Item)entity);
            }
        }
        return null;
    }
    public static MessageEmbed buildEntitiesMessage(List<BaseEntity> entities) {
        return new EmbedBuilder().setColor(Color.cyan)
                .setDescription(entities.stream().map(BaseEntity::getName).collect(Collectors.joining(", ")))
                .build();
    }
}
