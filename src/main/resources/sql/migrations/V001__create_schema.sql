create table mini_games (
  id          int not null auto_increment primary key,
  name        varchar(32),
  image       varchar(32)
);
create table devices (
  id          int not null auto_increment primary key,
  name        varchar(32),
  image       varchar(32)
);
create table items (
  id               int not null auto_increment primary key,
  name             varchar(32),
  image            varchar(32),
  description      varchar(256),
  item_type        varchar(32),
  console_type     varchar(32),
  data_holder_type varchar(32)
);
create table discord_highscores (
  id                int not null auto_increment primary key,
  discord_game_name varchar(32),
  discord_user_id   varchar(32),
  score             long
);

create view mini_games_view as (
  select
    id,
    name,
    image
  from mini_games
);

insert into mini_games (name, image)
select 'Gong' as name, 'https://i.imgur.com/0Bbzxau.png' as image
union all
select 'Pairs' as name, 'https://i.imgur.com/StRUHav.png' as image
union all
select 'Fizhy' as name, 'https://i.imgur.com/YSgHqfp.png' as image;

insert into devices (name, image)
select 'GameGuy' as name, 'https://i.imgur.com/uyETMJO.png' as image;

insert into items (name, image, description, item_type, console_type)
select 'GameGuy White' as name, 'https://i.imgur.com/uyETMJO.png' as image,
       'Portable game device with low specs.' as description,
       'Console' as item_type, 'GameGuy' as console_type;

insert into items (name, image, description, item_type, data_holder_type)
select 'GameGuy Cartridge Gong' as name, 'https://i.imgur.com/IeIqfJw.png' as image,
       'Game modul for the GameGuy handheld console with the game Gong.' as description,
       'DataHolder' as item_type, 'GameGuyCartridge' as data_holder_type;