Set Editor to use
  set KUBE_EDITOR=C:\Programs\Notepad++\notepad++.exe -multiInst -nosession -notabbar

Create Deployment
  kubectl create deployment gmkub --image=badtoxic/gm_kub:0.9.2

List Pods
  kubectl get pods
List Deployments
  kubectl get deployments
  
Edit Pods
  kubectl edit pod/gmkub-667b8f48dc-77dnx
  
List Images
  docker image ls

Login to Docker Hub
  docker login --username=name --password=pass
 maybe before:
  docker logout 
  
 or?
  gcloud container clusters get-credentials gamemaster-discord-bot-kubernetes --region europe-north1 --project gamemaster-discord-bot
  
Build docker image from Dockerfile
  docker build -t badtoxic/gm_kub:0.9.2 .
  
Upload image to Docker Hub
  docker push badtoxic/gm_kub:0.9.2
 or 
  gcloud docker -- push badtoxic/gm_kub:0.9.2

Set the new image for Kubernetes Deployment
  kubectl set image deployment/gmkub gmkub=badtoxic/gm_kub:0.9.2
 or 
  kubectl set image deployment/gmkub gm-kub-84h84=badtoxic/gm_kub:0.9.2
 (GC may have set a strange container name. Find it via kubectl describe pods) 

Restart all pods of a Deployment
  kubectl rollout restart deployment/gmkub

Create a configmap
  kubectl create configmap gmkub-config --from-file=gmkub-config

Show Log
  kubectl logs -l app=gmkub
oder (mit kubectl get pods)
  kubectl logs gmkub-f7b787666-6pqn7

Create Service
  kubectl expose deployment gmkub --port=8080
 or 
  kubectl expose deployment gmkub --type="LoadBalancer" --port 8080
    
Externe IP-Adresse finden  
  kubectl get service gmkub --watch