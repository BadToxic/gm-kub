![GameMaster Discord Bot Icon](https://gitlab.com/BadToxic/gm-kub/-/raw/main/img/icon-256.png)
# GameMaster Discord Bot

Join the BadToxic and/or GameMaster Discord Server if you want to try it and for news, info and support:

[BadToxic Discord Server](https://discord.gg/8QMCm2d)
[GameMaster Discord Server](http://discord.gg/Y9CsMmF)


# Snake Game:

[![Snake Game on YouTube](http://i3.ytimg.com/vi/QuFiCZeF4jA/hqdefault.jpg)](https://www.youtube.com/watch?v=QuFiCZeF4jA)


# About BadToxic:

- Discord (BadToxic Server) https://discord.gg/8QMCm2d
- Instagram Main https://www.instagram.com/xybadtoxic
- Instagram Dev https://www.instagram.com/badtoxicdev
- Twitter https://twitter.com/BadToxic
- YouTube https://www.youtube.com/user/BadToxic
- TikTok https://www.tiktok.com/@badtoxic
- Apple Developer https://itunes.apple.com/us/developer/michael-groenert/id955473699
- GooglePlay https://play.google.com/store/apps/developer?id=BadToxic

- More: Linktree https://linktr.ee/badtoxic
