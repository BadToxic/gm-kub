create table discord_highscores
(
	id int auto_increment,
	discord_game_name varchar(32) not null,
	discord_user_id varchar(32) not null,
	score long not null,
	constraint discord_highscores_pk
		primary key (id)
);

create table devices
(
	id int auto_increment,
	name varchar(32) not null,
	image varchar(32) null,
	constraint devices_pk
		primary key (id)
);

create table items
(
	id int auto_increment,
	name varchar(32) not null,
	image varchar(32) null,
	description varchar(256) null,
	item_type varchar(32) not null,
	console_type varchar(32) null,
	data_holder_type varchar(32) null,
	constraint items_pk
		primary key (id)
);

create table mini_games
(
	id int auto_increment,
	name varchar(32) not null,
	image varchar(32) null,
	constraint mini_games_pk
		primary key (id)
);
